//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Admin.Entities.SIM
{
    using System;
    using System.Collections.Generic;
    
    public partial class ADM_GetKomponenBaru
    {
        public string JasaID { get; set; }
        public string KomponenBiayaID { get; set; }
        public string KomponenName { get; set; }
        public string Harga { get; set; }
        public string HargaHC { get; set; }
    }
}
