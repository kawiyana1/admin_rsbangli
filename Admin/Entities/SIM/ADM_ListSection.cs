//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Admin.Entities.SIM
{
    using System;
    using System.Collections.Generic;
    
    public partial class ADM_ListSection
    {
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public string PenanggungJawab { get; set; }
        public string TipePelayanan { get; set; }
        public string UnitBisnisID { get; set; }
        public string UnitBisnisName { get; set; }
    }
}
