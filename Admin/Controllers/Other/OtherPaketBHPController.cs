﻿using Admin.Entities.SIM;
using Admin.Helper;
using Admin.Models.Other;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Admin.Controllers.Inventory
{
    [Authorize(Roles = "Admin")]
    public class OtherPaketBHPController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_ListPaketBHP.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.Kode.Contains(x.Value) ||
                                y.NamaPaket.Contains(x.Value) ||
                                y.Ditagihkan.Contains(x.Value) ||
                                y.Section_SectionName.Contains(x.Value));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.Kode,
                        Nama = m.NamaPaket,
                        Section = m.Section_SectionName,
                        Ditagihkan = m.Ditagihkan_Ket
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, OtherPaketBHPModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string id = "";
                        if (_process == "CREATE")
                        {
                            if (model.Detail == null) model.Detail = new List<OtherPaketBHPDetailModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            s.ADM_InsertPaketBHP(
                                model.Id,
                                model.Nama,
                                model.Section,
                                model.Ditagihkan,
                                model.IncludeJasa
                            );
                            id = model.Id;

                            foreach (var x in model.Detail)
                            {
                                s.ADM_InsertDetailPaketBHP(
                                    id,
                                    x.Id,
                                    x.Qty
                                );
                            }
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            if (model.Detail == null) model.Detail = new List<OtherPaketBHPDetailModel>();

                            s.ADM_UpdatePaketBHP(
                                model.Id,
                                model.Nama,
                                model.Section,
                                model.Ditagihkan,
                                model.IncludeJasa
                            );

                            var d = s.ADM_GetDetailPaketBHP.Where(x => x.KodePaket == model.Id).ToList();
                            foreach (var x in d)
                            {
                                var _d = model.Detail.FirstOrDefault(y => y.Id == x.Kode_Barang);
                                //delete
                                if (_d == null) s.ADM_DeleteDetailPaketBHP(model.Id, x.Kode_Barang);
                            }
                            foreach (var x in model.Detail)
                            {
                                var _d = d.FirstOrDefault(y => y.Kode_Barang == x.Id);
                                if (_d == null)
                                {
                                    // new
                                    s.ADM_InsertDetailPaketBHP(
                                        model.Id,
                                        x.Id,
                                        x.Qty
                                    );
                                }
                                else
                                {
                                    // edit
                                    s.ADM_UpdateDetailPaketBHP(
                                        model.Id,
                                        x.Id,
                                        x.Qty
                                    );
                                }

                            }
                            s.SaveChanges();
                        }
                        else if (_process == "DELETE")
                        {
                            var d = s.ADM_GetDetailPaketBHP.Where(x => x.KodePaket == model.Id).ToList();
                            foreach (var x in d)
                            {
                                s.ADM_DeleteDetailPaketBHP(model.Id, x.Kode_Barang);
                            }
                            s.ADM_DeletePaketBHP(model.Id);
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"OtherPaketBHP-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetPaketBHP.FirstOrDefault(x => x.Kode == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.ADM_GetDetailPaketBHP.Where(x => x.KodePaket == m.Kode).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Id = m.Kode,
                            Nama = m.NamaPaket,
                            Section = m.SectionID,
                            SectionNama = m.SectionName,
                            Ditagihkan = m.Ditagihkan,
                            IncludeJasa = m.IncludeJasa
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.Kode_Barang,
                            Nama = x.Nama_Barang,
                            Satuan = x.Satuan,
                            Qty = x.QTY,
                            Konversi = x.Konv
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P

        [HttpPost]
        public string ListLookupBarang(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_GetBarang.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.Nama_Barang.Contains(x.Value) ||
                                y.Kode_Barang.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Id = x.Barang_ID,
                        Kode = x.Kode_Barang,
                        Nama = x.Nama_Barang,
                        Konversi = x.Konversi,
                        Satuan = x.Satuan,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}