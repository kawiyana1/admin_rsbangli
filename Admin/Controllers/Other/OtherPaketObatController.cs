﻿using Admin.Entities.SIM;
using Admin.Helper;
using Admin.Models.Other;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Admin.Controllers.Inventory
{
    [Authorize(Roles = "Admin")]
    public class OtherPaketObatController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_ListPaketObat.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.KodePaket.Contains(x.Value) ||
                                y.NamaPaket.Contains(x.Value) ||
                                y.Section1_SectionName.Contains(x.Value));
                        }
                        else if (x.Key == "Dokter" && !string.IsNullOrEmpty(x.Value))
                        {
                            var id = int.Parse(x.Value);
                            proses = proses.Where(y => y.DokterID == id);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.KodePaket,
                        Nama = m.NamaPaket,
                        Section = m.Section1_SectionName,
                        LastUpdate = m.LastUpdate.ToString("dd/MM/yyyy"),
                        Dokter = m.Nama_Supplier
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, OtherPaketObatModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string id = "";
                        if (_process == "CREATE")
                        {
                            if (model.Detail == null) model.Detail = new List<OtherPaketObatDetailModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            id = s.ADM_InsertPaketObat(
                                model.Nama,
                                model.Section,
                                DateTime.Today,
                                model.Dokter,
                                model.ObatPuyer,
                                model.Keterangan
                            ).FirstOrDefault();

                            foreach (var x in model.Detail)
                            {
                                s.ADM_InsertDetailPaketObat(
                                    id,
                                    x.Id,
                                    x.Qty,
                                    x.AturanPakai
                                );
                            }
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            if (model.Detail == null) model.Detail = new List<OtherPaketObatDetailModel>();

                            s.ADM_UpdatePaketObat(
                                model.Id,
                                model.Nama,
                                model.Section,
                                DateTime.Today,
                                model.Dokter,
                                model.ObatPuyer,
                                model.Keterangan
                            );

                            var d = s.ADM_GetDetailPaketObat.Where(x => x.KodePaket == model.Id).ToList();
                            foreach (var x in d)
                            {
                                var _d = model.Detail.FirstOrDefault(y => y.Id == x.Barang_ID);
                                //delete
                                if (_d == null) s.ADM_DeleteDetailPaketObat(model.Id, x.Barang_ID);
                            }
                            foreach (var x in model.Detail)
                            {
                                var _d = d.FirstOrDefault(y => y.Barang_ID == x.Id);
                                if (_d == null)
                                {
                                    // new
                                    s.ADM_InsertDetailPaketObat(
                                        model.Id,
                                        x.Id,
                                        x.Qty,
                                        x.AturanPakai
                                    );
                                }
                                else
                                {
                                    // edit
                                    s.ADM_UpdateDetailPaketObat(
                                        model.Id,
                                        x.Id,
                                        x.Qty,
                                        x.AturanPakai
                                    );
                                }

                            }
                            s.SaveChanges();
                        }
                        else if (_process == "DELETE")
                        {
                            var d = s.ADM_GetDetailPaketObat.Where(x => x.KodePaket == model.Id).ToList();
                            foreach (var x in d) {
                                s.ADM_DeleteDetailPaketObat(model.Id, x.Barang_ID);
                            }
                            s.ADM_DeletePaketObat(model.Id);
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"OtherPaketObat-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetPaketObat.FirstOrDefault(x => x.KodePaket == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.ADM_GetDetailPaketObat.Where(x => x.KodePaket == m.KodePaket).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Id = m.KodePaket,
                            Nama = m.NamaPaket,
                            Section = m.SectionID,
                            SectionNama = m.SectionName,
                            Dokter = m.Supplier_ID,
                            DokterKode = m.Kode_Supplier,
                            DokterNama = m.NamaDokter,
                            ObatPuyer = m.Puyer,
                            Keterangan = m.Keterangan
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Nama = x.Nama_Barang,
                            Satuan = x.Satuan,
                            Qty = x.Qty,
                            AturanPakai = x.AturanPakai
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P

        [HttpPost]
        public string ListLookupBarang(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_GetBarang.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.Nama_Barang.Contains(x.Value) ||
                                y.Kode_Barang.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Id = x.Barang_ID,
                        Kode = x.Kode_Barang,
                        Nama = x.Nama_Barang,
                        Konversi = x.Konversi,
                        Satuan = x.Satuan,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}