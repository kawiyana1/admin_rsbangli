﻿using Admin.Entities.SIM;
using Admin.Helper;
using Admin.Models.Other;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Admin.Controllers.Inventory
{
    [Authorize(Roles = "Admin")]
    public class OtherDiskonController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_ListDiskon.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.IDDiscount.Contains(x.Value) ||
                                y.NamaDiscount.Contains(x.Value) ||
                                y.Rekening_Akun_No.Contains(x.Value) ||
                                y.Rekening_Akun_Name.Contains(x.Value));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.IDDiscount,
                        Nama = m.NamaDiscount,
                        AkunNo = m.Rekening_Akun_No,
                        AkunNama = m.Rekening_Akun_Name,
                        HaveD = m.HaveD
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, OtherDiskonModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string id = "";
                        if (_process == "CREATE")
                        {
                            if (model.Detail == null) model.Detail = new List<OtherDiskonDetailModel>();
                            s.ADM_InsertDiskon(
                                model.Id,
                                model.Nama,
                                model.Akun,
                                model.DenganPetugas,
                                model.DiskonKomponen,
                                model.NamaInternasional,
                                model.ExceptionJasa,
                                model.DiskonKomponenCheck,
                                model.DiskonGroupJasaCheck,
                                model.DiskonGroupJasa,
                                model.DiskonTotal,
                                model.DiskonTidakLangsung
                            ).FirstOrDefault();
                            id = model.Id;

                            foreach (var x in model.Detail)
                            {
                                s.ADM_InsertDetailJasaDiskon(
                                    id,
                                    x.Id
                                );
                            }
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            if (model.Detail == null) model.Detail = new List<OtherDiskonDetailModel>();

                            s.ADM_UpdateDiskon(
                                model.Id,
                                model.Nama,
                                model.Akun,
                                model.DenganPetugas,
                                model.DiskonKomponen,
                                model.NamaInternasional,
                                model.ExceptionJasa,
                                model.DiskonKomponenCheck,
                                model.DiskonGroupJasaCheck,
                                model.DiskonGroupJasa,
                                model.DiskonTotal,
                                model.DiskonTidakLangsung
                            );

                            var d = s.ADM_GetDetailJasaDiskon.Where(x => x.IDDiscount == model.Id).ToList();
                            foreach (var x in d)
                            {
                                var _d = model.Detail.FirstOrDefault(y => y.Id == x.IDJasa);
                                //delete
                                if (_d == null) s.ADM_DeleteDetailJasaDiskon(model.Id, x.IDJasa);
                            }
                            foreach (var x in model.Detail)
                            {
                                var _d = d.FirstOrDefault(y => y.IDJasa == x.Id);
                                if (_d == null)
                                {
                                    // new
                                    s.ADM_InsertDetailJasaDiskon(
                                        model.Id,
                                        x.Id
                                    );
                                }
                            }
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"OtherDiskon-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetDiskon.FirstOrDefault(x => x.IDDiscount == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.ADM_GetDetailJasaDiskon.Where(x => x.IDDiscount == m.IDDiscount).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Id = m.IDDiscount,
                            Nama = m.NamaDiscount,
                            Akun = m.Rekening_Akun_No,
                            AkunNama = m.Rekening_Akun_Name,
                            DenganPetugas = m.DenganPetugas,
                            DiskonKomponen = m.KomponenID,
                            DiskonKomponenNama = m.KomponenName,
                            NamaInternasional = m.NamaInternational,
                            ExceptionJasa = m.DenganJasa ?? false,
                            DiskonKomponenCheck =m.DiskonKomponen ?? false,
                            DiskonGroupJasaCheck = m.DiskonGroupJasa ?? false,
                            DiskonGroupJasa = m.GroupID,
                            DiskonGroupJasaNama = m.GroupName,
                            DiskonTotal = m.DiskonTotal ?? false,
                            DiskonTidakLangsung = m.DiskonTdkLangsung ?? false
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.IDJasa,
                            Nama = x.JasaName
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P

        [HttpPost]
        public string ListLookupJasa(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_DiskonJasa.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.JasaID.Contains(x.Value) ||
                                y.JasaName.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Id = x.JasaID,
                        Nama = x.JasaName,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}