﻿using Admin.Entities.SIM;
using Admin.Helper;
using Admin.Models.Other;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Inventory
{
    [Authorize(Roles = "Admin")]
    public class OtherGradingHargaObatController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var userid = User.Identity.GetUserId();
                    var proses = s.ADM_ListGradingHargaObat.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.JenisKerjasama.Contains(x.Value) ||
                                y.NamaKelas.Contains(x.Value) || 
                                y.KelompokJenis.Contains(x.Value) || 
                                y.Golongan.Contains(x.Value)
                            );
                        else if (x.Key == "TipePelayanan" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y => y.TipePelayanan == x.Value);
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Id_TipePelayanan = x.TipePelayanan,
                        Id_TipePasien = x.JenisKerjasamaID,
                        Id_Kelas = x.KelasID,
                        Id_KelompokGradingObat = x.KelompokJenis,
                        Id_KTP = x.KTP,
                        Id_RangeHarga1 = x.StartHarga,
                        TipePasien = x.JenisKerjasama,
                        Kelas = x.NamaKelas,
                        x.KTP,
                        RangeHarga1 = x.StartHarga,
                        RangeHarga2 = x.EndHarga,
                        PresentaseUp = x.ProsentaseUp,
                        KelompokGradingObat = x.KelompokJenis,
                        GolonganObat = x.Golongan,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, OtherGradingHargaObatModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string id = "";
                        if (_process == "CREATE")
                        {
                            s.ADM_InsertGradingHargaObat(
                                model.TipePelayanan,
                                model.TipePasien,
                                model.Kelas,
                                model.GolonganObat,
                                model.KelompokGradingObat,
                                model.KTP ? (byte)1 : (byte)0,
                                model.RangeHarga1,
                                model.RangeHarga2,
                                model.PresentaseUp,
                                model.Diskon,
                                model.PPN,
                                model.TglMulaiBerlaku
                            );
                            id = $"{model.TipePelayanan} {model.TipePasien} {model.Kelas} {model.Id_KTP} {model.Id_RangeHarga1} {model.KelompokGradingObat}";
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            s.ADM_UpdateGradingHargaObat(
                                model.Id_TipePelayanan,
                                model.Id_TipePasien,
                                model.Id_Kelas,
                                model.GolonganObat,
                                model.Id_KelompokGradingObat,
                                model.Id_KTP ? (byte)1 : (byte)0,
                                model.Id_RangeHarga1,
                                model.RangeHarga2,
                                model.PresentaseUp,
                                model.Diskon,
                                model.PPN,
                                model.TglMulaiBerlaku
                            );
                            s.SaveChanges();
                            id = $"{model.Id_TipePelayanan} {model.Id_TipePasien} {model.Id_Kelas} {model.Id_KTP} {model.Id_RangeHarga1} {model.Id_KelompokGradingObat}";
                        }
                        else if (_process == "DELETE")
                        {
                            s.ADM_DeleteGradingHargaObat(
                                model.Id_TipePelayanan,
                                model.Id_TipePasien,
                                model.Id_Kelas,
                                model.Id_KTP,
                                model.Id_RangeHarga1,
                                model.Id_KelompokGradingObat
                            );
                            s.SaveChanges();
                            id = $"{model.Id_TipePelayanan} {model.Id_TipePasien} {model.Id_Kelas} {model.Id_KTP} {model.Id_RangeHarga1} {model.Id_KelompokGradingObat}";
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"OtherGradingHargaObat-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
            return HConvert.Success();
        }

        [HttpPost]
        public string Detail(string Id_TipePelayanan, int Id_TipePasien, string Id_Kelas, string Id_KelompokGradingObat, bool Id_KTP, decimal Id_RangeHarga1)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var userid = User.Identity.GetUserId();

                    byte ktp = Id_KTP ? (byte)1 : (byte)0;
                    var m = s.ADM_GetGradingHargaObat.FirstOrDefault(x => 
                        x.TipePelayanan == Id_TipePelayanan && 
                        x.JenisKerjasamaID == Id_TipePasien &&
                        x.KelasID == Id_Kelas &&
                        x.KelompokJenis == Id_KelompokGradingObat &&
                        x.KTP == ktp &&
                        x.StartHarga == Id_RangeHarga1
                    );
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Id_TipePelayanan = m.TipePelayanan,
                            Id_TipePasien = m.JenisKerjasamaID,
                            Id_Kelas = m.KelasID,
                            Id_KelompokGradingObat = m.KelompokJenis,
                            Id_KTP = m.KTP == 1,
                            Id_RangeHarga1 = m.StartHarga,

                            TipePelayanan = m.TipePelayanan,
                            TipePasien = m.JenisKerjasamaID,
                            Kelas = m.KelasID,
                            GolonganObat = m.Golongan,
                            KelompokGradingObat = m.KelompokJenis,
                            KTP = m.KTP == 1,
                            RangeHarga1 = m.StartHarga,
                            RangeHarga2 = m.EndHarga,
                            PresentaseUp = m.ProsentaseUp,
                            Diskon = m.ProsentaseDiscount,
                            PPN = m.PPN,
                            TglMulaiBerlaku = m.TglBerlaku?.ToString("yyyy-MM-dd"),
                        }
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}