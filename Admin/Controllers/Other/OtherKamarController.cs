﻿using Admin.Entities.SIM;
using Admin.Helper;
using Admin.Models.Other;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Admin.Controllers.Inventory
{
    [Authorize(Roles = "Admin")]
    public class OtherKamarController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_ListKamar.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.NoKamar.Contains(x.Value) ||
                                y.NamaKamar.Contains(x.Value) ||
                                y.Section_SectionName.Contains(x.Value) ||
                                y.Kelas_NamaKelas.Contains(x.Value));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.NoKamar,
                        NamaKamar = m.NamaKamar,
                        Sal = m.Section_SectionName,
                        JmlBed = m.JmlBed,
                        NoLantai = m.NoLantai,
                        Tambahan = m.Tambahan,
                        Kelas = m.Kelas_NamaKelas
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, OtherKamarModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string id = model.Id;
                        if (_process == "CREATE")
                        {
                            if (model.Detail == null) model.Detail = new List<OtherKamarDetailModel>();
                            if (model.Detail.Count == 0) throw new Exception("Detail tidak boleh kosong");
                            id = s.ADM_InsertKamar(
                                model.NamaKamar,
                                model.Sal,
                                model.JmlBed,
                                model.NoLantai,
                                model.Tambahan,
                                model.Kelas
                            ).FirstOrDefault();

                            foreach (var x in model.Detail)
                            {
                                s.ADM_InsertDetailKamar(
                                    id,
                                    x.Id
                                );
                            }
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            if (model.Detail == null) model.Detail = new List<OtherKamarDetailModel>();

                            s.ADM_UpdateKamar(
                                model.NoKamar,
                                model.Sal,
                                model.JmlBed,
                                model.NoLantai,
                                model.Tambahan,
                                model.Kelas,
                                model.NamaKamar
                            );

                            var d = s.ADM_GetDetailKamar.Where(x => x.NoKamar == model.NoKamar).ToList();
                            foreach (var x in d)
                            {
                                var _d = model.Detail.FirstOrDefault(y => y.Id == x.NoBed);
                                //delete
                                if (_d == null) s.ADM_DeleteDetailKamar(model.NoKamar, x.NoBed);
                            }
                            foreach (var x in model.Detail)
                            {
                                var _d = d.FirstOrDefault(y => y.NoBed == x.Id);
                                if (_d == null)
                                {
                                    // new
                                    s.ADM_InsertDetailKamar(
                                        model.NoKamar,
                                        x.Id
                                    );
                                }
                            }
                            s.SaveChanges();
                        }
                        else if (_process == "DELETE")
                        {
                            var d = s.ADM_GetDetailKamar.Where(x => x.NoBed == model.Id).ToList();
                            foreach (var x in d)
                            {
                                s.ADM_DeleteDetailKamar(model.Id, x.NoBed);
                            }
                            s.ADM_DeleteKamar(model.NoKamar);
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"OtherKamar-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_ListKamar.FirstOrDefault(x => x.NoKamar == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.ADM_GetDetailKamar.Where(x => x.NoKamar == m.NoKamar).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Id = m.NoKamar,
                            NoKamar = m.NoKamar,
                            NamaKamar = m.NamaKamar,
                            SalID = m.SalID,
                            SalName = m.Section_SectionName,
                            JmlBed = m.JmlBed,
                            NoLantai = m.NoLantai,
                            Tambahan = m.Tambahan,
                            KelasName = m.Kelas_NamaKelas,
                            Kelas = m.KelasID
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Id = x.NoBed,
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}