﻿using Admin.Entities.SIM;
using Admin.Helper;
using Admin.Models.Other;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Inventory.Controllers.Inventory
{
    [Authorize(Roles = "Admin")]
    public class OtherJamWaktuPraktekController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var userid = User.Identity.GetUserId();
                    var proses = s.ADM_ListJamPraktek.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.NamaWaktu.Contains(x.Value)
                            );
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Id = x.WaktuID,
                        Nama = x.NamaWaktu,
                        DariJam = $"{x.FromJam:HH}:{x.FromJam:mm}",
                        SampaiJam = $"{x.ToJam:HH}:{x.ToJam:mm}"
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, OtherJamWaktuPraktekModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        int id = 0;
                        if (_process == "CREATE")
                        {
                            id = s.ADM_InsertJamPraktek(
                                model.Nama,
                                DateTime.Today + model.DariJam,
                                DateTime.Today + model.SampaiJam,
                                model.Keterangan
                            );
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            s.ADM_UpdateJamPraktek(
                                model.Id,
                                model.Nama,
                                DateTime.Today + model.DariJam,
                                DateTime.Today + model.SampaiJam,
                                model.Keterangan
                            );
                            s.SaveChanges();
                            id = model.Id;
                        }
                        else if (_process == "DELETE")
                        {
                            s.ADM_DeleteJamPraktek(model.Id);
                            s.SaveChanges();
                            id = model.Id;
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"OtherJamWaktuPraktek-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
            return HConvert.Success();
        }

        [HttpPost]
        public string Detail(int id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var userid = User.Identity.GetUserId();

                    var m = s.ADM_GetJamPraktek.FirstOrDefault(x => x.WaktuID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Id = m.WaktuID,
                            Nama = m.NamaWaktu,
                            DariJam = $"{m.FromJam:HH}:{m.FromJam:mm}",
                            SampaiJam = $"{m.ToJam:HH}:{m.ToJam:mm}",
                            Keterangan = m.Keterangan,
                        }
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}