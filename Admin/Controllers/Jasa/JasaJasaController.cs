﻿using Admin.Entities.SIM;
using Admin.Helper;
using Admin.Models.Jasa;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Admin.Controllers.Jasa
{
    [Authorize(Roles = "Admin")]
    public class JasaJasaController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_ListJasa.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.JasaID.Contains(x.Value) ||
                                y.JasaName.Contains(x.Value) ||
                                y.GroupJasa_GroupJasaName.Contains(x.Value) ||
                                y.KategoriBiaya_KategoriJasaName.Contains(x.Value));
                        }
                        else if (x.Key == "Aktif" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y => y.Aktif == (x.Value == "TRUE"));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.JasaID,
                        Nama = m.JasaName,
                        GroupJasa = m.GroupJasa_GroupJasaName,
                        KategoriJasa = m.KategoriBiaya_KategoriJasaName
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, JasaJasaModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        string id = "";

                        // validasi detail section
                        if (model.DetailSection == null) model.DetailSection = new List<JasaJasaDetailSectionModel>();
                        if (model.DetailSection.GroupBy(x => x.Id).Select(x => x.Count()).Where(x => x > 1).Count() > 0)
                            throw new Exception("Detail Section tidak boleh sama");

                        // validasi detail data lab
                        if (model.DetailDataLAB == null) model.DetailDataLAB = new List<JasaJasaDetailDataLABModel>();
                        if (model.DetailDataLAB.GroupBy(x => x.Id).Select(x => x.Count()).Where(x => x > 1).Count() > 0)
                            throw new Exception("Data Lab tidak boleh sama");

                        // validasi harga umum
                        if (model.DetailHargaUmum == null) model.DetailHargaUmum = new List<JasaJasaDetailHargaUmumModel>();
                        foreach (var x in model.DetailHargaUmum) 
                        {
                            if (x.Dokter == null) throw new Exception("Detail Harga Umum Dokter harus diisi");
                            if (x.TglBerlaku == null) throw new Exception("Detail Harga Umum Tgl Berlaku harus diisi");
                        }

                        if (_process == "CREATE")
                        {
                            if (model.DetailHargaUmum == null) model.DetailHargaUmum = new List<JasaJasaDetailHargaUmumModel>();
                            if (model.DetailBHP == null) model.DetailBHP = new List<JasaJasaDetailBHPModel>();
                            if (model.DetailSection == null) model.DetailSection = new List<JasaJasaDetailSectionModel>();
                            if (model.DetailDataLAB == null) model.DetailDataLAB = new List<JasaJasaDetailDataLABModel>();
                            id = s.ADM_InsertHeaderJasa(
                                model.Nama,
                                model.NamaInternasional,
                                model.GroupJasa,
                                model.KategoriJasa,
                                model.KategoriPlafon,
                                model.KeteranganDiInvoice,
                                model.Aktif,
                                model.MengunakanAlatPenunjang,
                                model.TarifManual,
                                model.MappingPemakaianAlat,
                                model.KelompokPostinganBy,
                                model.SumberInsentif,
                                model.HariRawatInap,
                                model.AutoSystem,
                                model.MCU,
                                model.KSO,
                                model.VariableKategoriOperasi,
                                model.VariableCito,
                                model.HarusDenganDokterPetugas,
                                model.JasaPaket,
                                model.Poliklinik,
                                model.CostRS,
                                model.CostRSRp
                            ).FirstOrDefault();

                            foreach (var x in model.DetailHargaUmum)
                            {
                                var _id = s.ADM_InsertDetailHargaUmum(
                                    id,
                                    x.Kelas,
                                    x.TipePasien,
                                    x.KTP,
                                    x.KatOperasi,
                                    x.Dokter,
                                    x.HargaLama,
                                    x.HargaBaru,
                                    x.HargaLama,
                                    x.HargaHC,
                                    x.TglBerlaku,
                                    x.Spesialis,
                                    x.Cito,
                                    x.Lokasi,
                                    x.DiscHC,
                                    x.SubSpesialis,
                                    x.HargaBPJS,
                                    x.HargaBPJSLama,
                                    x.TglBerlakuBPJS,
                                    x.InsentifKomponen,
                                    x.InsentifDetail
                                ).FirstOrDefault();

                                if (x.Detail == null) x.Detail = new List<JasaJasaDetailHargaUmumDetailModel>();
                                foreach (var y in x.Detail)
                                {
                                    s.ADM_InsertKomponenBiaya(
                                        _id,
                                        y.Id,
                                        y.Qty,
                                        y.HargaLama,
                                        y.HargaBaru,
                                        y.HargaHCLama,
                                        y.HargaHC,
                                        y.UsePersen,
                                        y.Persen,
                                        y.AkunHPP,
                                        y.Insentif,
                                        y.HargaAwal,
                                        y.HargaLama,
                                        y.IncInsent,
                                        y.DiscInsentifHC,
                                        y.AkunLawan,
                                        y.HargaBPJS,
                                        y.HargaBPJSLama
                                    );
                                }
                            }
                            foreach (var x in model.DetailBHP)
                            {
                                s.ADM_InsertDetailBHPJasa(
                                    id,
                                    x.Id,
                                    x.Qty,
                                    x.Ditagihkan
                                );
                            }
                            foreach (var x in model.DetailSection)
                            {
                                s.ADM_InsertDetailSectionJasa(
                                    id,
                                    x.Id
                                );
                            }
                            foreach (var x in model.DetailDataLAB)
                            {
                                s.ADM_InsertDetailDataLabJasa(
                                    id,
                                    x.Id
                                );
                            }

                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            if (model.DetailHargaUmum == null) model.DetailHargaUmum = new List<JasaJasaDetailHargaUmumModel>();
                            if (model.DetailBHP == null) model.DetailBHP = new List<JasaJasaDetailBHPModel>();
                            if (model.DetailSection == null) model.DetailSection = new List<JasaJasaDetailSectionModel>();
                            if (model.DetailDataLAB == null) model.DetailDataLAB = new List<JasaJasaDetailDataLABModel>();

                            s.ADM_UpdateHeaderJasa(
                                model.Id,
                                model.Nama,
                                model.NamaInternasional,
                                model.GroupJasa,
                                model.KategoriJasa,
                                model.KategoriPlafon,
                                model.KeteranganDiInvoice,
                                model.Aktif,
                                model.MengunakanAlatPenunjang,
                                model.TarifManual,
                                model.MappingPemakaianAlat,
                                model.KelompokPostinganBy,
                                model.SumberInsentif,
                                model.HariRawatInap,
                                model.AutoSystem,
                                model.MCU,
                                model.KSO,
                                model.VariableKategoriOperasi,
                                model.VariableCito,
                                model.HarusDenganDokterPetugas,
                                model.JasaPaket,
                                model.Poliklinik,
                                model.CostRS,
                                model.CostRSRp
                            );

                            #region Harga Umum
                            var d_HargaUmum = s.ADM_GetDetailHargaUmum.Where(x => x.JasaID == model.Id).ToList();
                            foreach (var x in d_HargaUmum)
                            {
                                var _d = model.DetailHargaUmum.FirstOrDefault(y => y.Id == x.ListHargaID);
                                //delete
                                if (_d == null) s.ADM_DeleteDetailHargaUmum((int)x.ListHargaID);
                            }
                            foreach (var x in model.DetailHargaUmum)
                            {
                                var _d = d_HargaUmum.FirstOrDefault(y => y.ListHargaID == x.Id);
                                if (_d == null)
                                {
                                    // new
                                    var _id = s.ADM_InsertDetailHargaUmum(
                                        model.Id,
                                        x.Kelas,
                                        x.TipePasien,
                                        x.KTP,
                                        x.KatOperasi,
                                        x.Dokter,
                                        x.HargaLama,
                                        x.HargaBaru,
                                        x.HargaLama,
                                        x.HargaHC,
                                        x.TglBerlaku,
                                        x.Spesialis,
                                        x.Cito,
                                        x.Lokasi,
                                        x.DiscHC,
                                        x.SubSpesialis,
                                        x.HargaBPJS,
                                        x.HargaBPJSLama,
                                        x.TglBerlakuBPJS,
                                        x.InsentifKomponen,
                                        x.InsentifDetail
                                    ).FirstOrDefault();

                                    if (x.Detail == null) x.Detail = new List<JasaJasaDetailHargaUmumDetailModel>();
                                    foreach (var y in x.Detail)
                                    {
                                        s.ADM_InsertKomponenBiaya(
                                            _id,
                                            y.Id,
                                            y.Qty,
                                            y.HargaLama,
                                            y.HargaBaru,
                                            y.HargaHCLama,
                                            y.HargaHC,
                                            y.UsePersen,
                                            y.Persen,
                                            y.AkunHPP,
                                            y.Insentif,
                                            y.HargaAwal,
                                            y.HargaLama,
                                            y.IncInsent,
                                            y.DiscInsentifHC,
                                            y.AkunLawan,
                                            y.HargaBPJS,
                                            y.HargaBPJSLama
                                        );
                                    }
                                }
                                else
                                {
                                    // edit
                                    s.ADM_UpdateDetailHargaUmum(
                                        x.Id,
                                        x.Kelas,
                                        x.TipePasien,
                                        x.KTP,
                                        x.KatOperasi,
                                        x.Dokter,
                                        x.HargaLama,
                                        x.HargaBaru,
                                        x.HargaLama,
                                        x.HargaHC,
                                        x.TglBerlaku,
                                        x.Spesialis,
                                        x.Cito,
                                        x.Lokasi,
                                        x.DiscHC,
                                        x.SubSpesialis,
                                        x.HargaBPJS,
                                        x.HargaBPJSLama,
                                        x.TglBerlakuBPJS,
                                        x.InsentifKomponen,
                                        x.InsentifDetail
                                    );

                                    var d = s.ADM_GetKomponenBiaya.Where(y => y.ListHargaID == x.Id).ToList();
                                    foreach (var z in d)
                                    {
                                        var _d2 = x.Detail.FirstOrDefault(y => y.Id == z.KomponenBiayaID);
                                        //delete
                                        if (_d2 == null) s.ADM_DeleteKomponenBiaya(x.Id, z.KomponenBiayaID);
                                    }
                                    if (x.Detail == null) x.Detail = new List<JasaJasaDetailHargaUmumDetailModel>();
                                    foreach (var z in x.Detail)
                                    {
                                        var _d2 = d.FirstOrDefault(y => y.KomponenBiayaID == z.Id);
                                        if (_d2 == null)
                                        {
                                            // new
                                            s.ADM_InsertKomponenBiaya(
                                                x.Id,
                                                z.Id,
                                                z.Qty,
                                                z.HargaLama,
                                                z.HargaBaru,
                                                z.HargaHCLama,
                                                z.HargaHC,
                                                z.UsePersen,
                                                z.Persen,
                                                z.AkunHPP,
                                                z.Insentif,
                                                z.HargaAwal,
                                                z.HargaLama,
                                                z.IncInsent,
                                                z.DiscInsentifHC,
                                                z.AkunLawan,
                                                z.HargaBPJS,
                                                z.HargaBPJSLama
                                            );
                                        }
                                        else
                                        {
                                            // edit
                                            s.ADM_UpdateKomponenBiaya(
                                                x.Id,
                                                z.Id,
                                                z.Qty,
                                                z.HargaLama,
                                                z.HargaBaru,
                                                z.HargaHCLama,
                                                z.HargaHC,
                                                z.UsePersen,
                                                z.Persen,
                                                z.AkunHPP,
                                                z.Insentif,
                                                z.HargaAwal,
                                                z.HargaLama,
                                                z.IncInsent,
                                                z.DiscInsentifHC,
                                                z.AkunLawan,
                                                z.HargaBPJS,
                                                z.HargaBPJSLama
                                            );
                                        }

                                    }
                                }
                            }
                            #endregion

                            #region BHP
                            var d_BHP = s.ADM_GetDetailBHPJasa.Where(x => x.JasaID == model.Id).ToList();
                            foreach (var x in d_BHP)
                            {
                                var _d = model.DetailBHP.FirstOrDefault(y => y.Id == x.Kode_Barang);
                                //delete
                                if (_d == null) s.ADM_DeleteDetailBHPJasa(model.Id, x.Kode_Barang);
                            }
                            foreach (var x in model.DetailBHP)
                            {
                                var _d = d_BHP.FirstOrDefault(y => y.Kode_Barang == x.Id);
                                if (_d == null)
                                {
                                    // new
                                    s.ADM_InsertDetailBHPJasa(
                                        model.Id,
                                        x.Id,
                                        x.Qty,
                                        x.Ditagihkan
                                    );
                                }
                                else
                                {
                                    // edit
                                    s.ADM_UpdateDetailBHPJasa(
                                        model.Id,
                                        x.Id,
                                        x.Qty,
                                        x.Ditagihkan
                                    );
                                }
                            }
                            #endregion

                            #region Section
                            var d_Section = s.ADM_GetDetailSectionJasa.Where(x => x.JasaID == model.Id).ToList();
                            foreach (var x in d_Section)
                            {
                                var _d = model.DetailSection.FirstOrDefault(y => y.Id == x.SectionID);
                                //delete
                                if (_d == null) s.ADM_DeleteDetailSectionJasa(model.Id, x.SectionID);
                            }
                            foreach (var x in model.DetailSection)
                            {
                                var _d = d_Section.FirstOrDefault(y => y.SectionID == x.Id);
                                if (_d == null)
                                {
                                    // new
                                    s.ADM_InsertDetailSectionJasa(
                                        model.Id,
                                        x.Id
                                    );
                                }
                            }
                            #endregion

                            #region DataLAB
                            var d_DataLAB = s.ADM_GetDetailDataLabJasa.Where(x => x.JasaID == model.Id).ToList();
                            foreach (var x in d_DataLAB)
                            {
                                var _d = model.DetailDataLAB.FirstOrDefault(y => y.Id == x.TestID);
                                //delete
                                if (_d == null) s.ADM_DeleteDetailDataLabJasa(model.Id, x.TestID);
                            }
                            foreach (var x in model.DetailDataLAB)
                            {
                                var _d = d_DataLAB.FirstOrDefault(y => y.TestID == x.Id);
                                if (_d == null)
                                {
                                    // new
                                    s.ADM_InsertDetailDataLabJasa(
                                        model.Id,
                                        x.Id
                                    );
                                }
                            }
                            #endregion

                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"JasaJasa-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetHeaderJasa.FirstOrDefault(x => x.JasaID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d_HargaUmum = s.ADM_GetDetailHargaUmum.Where(x => x.JasaID == m.JasaID).ToList();
                    var listid = d_HargaUmum.Select(x => x.ListHargaID).ToList();
                    var d_HargaUmum_Detail = s.ADM_GetKomponenBiaya.Where(x => listid.Contains(x.ListHargaID)).ToList();
                    var d_BHP = s.ADM_GetDetailBHPJasa.Where(x => x.JasaID == m.JasaID).ToList();
                    var d_Section = s.ADM_GetDetailSectionJasa.Where(x => x.JasaID == m.JasaID).ToList();
                    var d_DataLAB = s.ADM_GetDetailDataLabJasa.Where(x => x.JasaID == m.JasaID).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Id = m.JasaID,
                            Nama = m.JasaName,
                            NamaInternasional = m.JasaNameEnglish,
                            GroupJasa = m.GroupJasaID,
                            GroupJasaNama = m.GroupJasaName,
                            KategoriJasa = m.KategoriJasaID,
                            KategoriJasaNama = m.KategoriJasaName,
                            KategoriPlafon = m.KategoriPlafon,
                            KeteranganDiInvoice = m.Keterangan,
                            Aktif = m.Aktif,
                            MengunakanAlatPenunjang = m.MenggunakanAlat,
                            TarifManual = m.Manual,
                            MappingPemakaianAlat = m.IDAlat,
                            KelompokPostinganBy = m.KelompokPostingan,
                            SumberInsentif = m.ModelInsentif,
                            HariRawatInap = m.HariRawatInap,
                            AutoSystem = m.AutoSystemRI,
                            MCU = m.MCU,
                            KSO = m.KSO,
                            VariableKategoriOperasi = m.Var_KategoriOperasi,
                            VariableCito = m.Var_Cito,
                            HarusDenganDokterPetugas = m.WithDokter,
                            JasaPaket = m.Paket,
                            Poliklinik = m.Poliklinik,
                            CostRS = m.CostRSPersen,
                            CostRSRp = m.CostRSRupiah
                        },
                        DetailHargaUmum = d_HargaUmum.ConvertAll(x => new
                        {
                            Id = x.ListHargaID,
                            Kelas = x.KelasID,
                            TipePasien = x.JenisPasienID,
                            Spesialis = x.SpesialisID,
                            SubSpesialis = x.SubSpesialisID,
                            Dokter = x.DokterID,
                            DokterNama = x.NamaDOkter,
                            KatOperasi = x.KategoriOperasiID,
                            Cito = x.Cyto,
                            KTP = x.PasienKTP,
                            Lokasi = x.Lokasi,
                            HargaLama = x.Harga_Lama,
                            HargaBaru = x.Harga_Baru,
                            HargaLamaHC = x.HargaHC_Lama,
                            HargaHC = x.HargaHC_Baru,
                            DiscHC = x.DiscHCUmum,
                            TglBerlaku = x.TglHargaBaru.ToString("yyyy-MM-dd"),
                            HargaBPJS = x.HargaBPJS,
                            HargaBPJSLama = x.HargaBPJS_Lama,
                            TglBerlakuBPJS = x.TglHargaBaruBPJS?.ToString("yyyy-MM-dd"),
                            InsentifDetail = x.InsentifDetail,
                            InsentifKomponen = x.InsentifKomponen,
                            Detail = d_HargaUmum_Detail.Where(y => y.ListHargaID == x.ListHargaID).ToList().ConvertAll(y => new {
                                Id = y.KomponenBiayaID,
                                Nama = y.KomponenName,
                                Qty = y.Qty,
                                //UsePersen = y.per,
                                Persen = y.NilaiPersen,
                                Insentif = y.PersenInsentif,
                                IncInsent = y.IncludeInsentif,
                                HargaAwal = y.HargaAwal,
                                HargaLama = y.HargaLama,
                                HargaBaru = y.HargaBaru,
                                HargaHCLama = y.HargaHCLama,
                                HargaHC = y.HargaHCBaru,
                                DiscInsentifHC = y.PersenInsentif,
                                AkunHPP = y.AkunNo,
                                AkunLawan = y.AkunNoLawan,
                                AkunHPPNama = y.AkunNo_Name,
                                AkunLawanNama = y.AkunNoLawan_Name,
                                HargaBPJS = y.HargaBPJS,
                                HargaBJPSLama = y.HargaBPJS_Lama,
                            })
                        }),
                        DetailBHP = d_BHP.ConvertAll(x => new
                        {
                            Id = x.Kode_Barang,
                            Nama = x.Nama_Barang,
                            Satuan = x.Satuan_Stok,
                            Qty = x.Qty,
                            Ditagihkan = x.Ditagihkan
                        }),
                        DetailSection = d_Section.ConvertAll(x => new
                        {
                            Id = x.SectionID,
                            Nama = x.SectionName,
                        }),
                        DetailDataLAB = d_DataLAB.ConvertAll(x => new
                        {
                            Id = x.TestID,
                            Nama = x.NamaTest,
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P

        [HttpPost]
        public string ListLookupBiayaKomponen(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_ListKomponenJasa.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.KomponenBiayaID.Contains(x.Value) ||
                                y.KomponenName.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Id = x.KomponenBiayaID,
                        Nama = x.KomponenName,
                        Insentif = x.IncludeInsentif,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string ListLookupBHP(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_GetLookUpDetailBHP.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.Kode_Barang.Contains(x.Value) ||
                                y.Nama_Kategori.Contains(x.Value) ||
                                y.Nama_Barang.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Id = x.Kode_Barang,
                        Nama = x.Nama_Barang,
                        Kategori = x.Nama_Kategori,
                        Satuan = x.Nama_Satuan,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}