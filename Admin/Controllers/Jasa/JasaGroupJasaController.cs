﻿using Admin.Entities.SIM;
using Admin.Helper;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using System.Linq.Dynamic;
using Admin.Models.Jasa;

namespace Admin.Controllers.Jasa
{
    public class JasaGroupJasaController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_ListGroupJasa.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y => 
                            y.GroupJasaID.ToString().Contains(x.Value) ||
                            y.NamaInternational.ToString().Contains(x.Value) ||
                            y.GroupJasaName.Contains(x.Value));

                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "DESC" : "ASC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        GroupJasaID = x.GroupJasaID,
                        GroupJasaName = x.GroupJasaName,
                        KelompokPerawatan = x.KelompokPerawatan,
                        NamaInternational = x.NamaInternational,


                    }); ;
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, JasaGroupJasaModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        var id = 0;
                        if (_process == "CREATE")
                        {
                            id = s.ADM_InsertGroupJasa(model.GroupJasaName, model.KelompokPerawatan, model.NamaInternational, model.AkunNoRI, model.AkunNoRJ, model.AKunNoUGD, model.AkunNoOnCall, model.AkunNoMA);
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            s.ADM_UpdateGroupJasa(model.GroupJasaID, model.GroupJasaName, model.KelompokPerawatan, model.NamaInternational, model.AkunNoRI, model.AkunNoRJ, model.AKunNoUGD, model.AkunNoOnCall, model.AkunNoMA);
                            s.SaveChanges();
                        }
                        else if (_process == "DELETE")
                        {
                            s.ADM_DeleteGroupJasa(model.GroupJasaID);
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SetupGroupJasa-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
            return HConvert.Success();
        }

        [HttpPost]
        public string Detail(byte id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetGroupJasa.FirstOrDefault(x => x.GroupJasaID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            GroupJasaID = m.GroupJasaID,
                            GroupJasaName = m.GroupJasaName,
                            KelompokPerawatan = m.KelompokPerawatan,
                            AkunNoMA = m.AkunNoMA,
                            AkunNoMAName = m.AkunNoMAName,
                            AkunNoOnCall = m.AkunNoOnCall,
                            AkunNoOnCallName = m.AkunNoOnCallName,
                            AkunNoRI = m.AkunNoRI,
                            AkunNoRIName = m.AkunNoRIName,
                            AkunNoRJ = m.AkunNoRJ,
                            AkunNoRJName = m.AkunNoRJName,
                            AkunNoUGD = m.AKunNoUGD,
                            AkunNoUGDName = m.AkunNoUGDName,
                            HonorDokter = m.HonorDokter,
                            Incentive = m.Incentive,
                            Kelompok_Kwitansi = m.Kelompok_Kwitansi,
                            NamaInternational = m.NamaInternational,
                            NoUrut = m.NoUrut,
                            SumberData = m.SumberData,
                            THT = m.THT,

                        }
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}