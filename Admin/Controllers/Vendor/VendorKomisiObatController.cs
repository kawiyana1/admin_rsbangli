﻿using Admin.Entities.SIM;
using Admin.Helper;
using Admin.Models.Vendor;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Admin.Controllers.Vendor
{
    [Authorize(Roles = "Admin")]
    public class VendorKomisiObatController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_ListKomisiObat.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.Kode_Supplier.Contains(x.Value) ||
                                y.Nama_Supplier.Contains(x.Value) ||
                                y.Alamat_1.Contains(x.Value));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.Supplier_ID,
                        Kode = m.Kode_Supplier,
                        Nama = m.Nama_Supplier,
                        Alamat = m.Alamat_1,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, VendorKomisiObatModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        int id = 0;
                        if (_process == "EDIT")
                        {
                            if (model.DetailObat == null) model.DetailObat = new List<VendorKomisiObatDetailObatModel>();
                            if (model.DetailTHTDokter == null) model.DetailTHTDokter = new List<VendorKomisiObatDetailTHTDokterModel>();
                            if (model.DetailTipePasien == null) model.DetailTipePasien = new List<VendorKomisiObatDetailTipePasienModel>();

                            s.ADM_UpdateKomisiObat(
                                model.Id,
                                model.TglMulaiTHT,
                                model.SelesaiTHT,
                                model.TglSelesaiTHT
                            );

                            #region TipePasien
                            var d_TipePasien = s.ADM_GetDetailTipePasienKomisiObat.Where(x => x.DokterID == model.Id).ToList();
                            foreach (var x in d_TipePasien)
                            {
                                var _d = model.DetailTipePasien.FirstOrDefault(y => y.Id == x.JenisPasienID);
                                //delete
                                if (_d == null) s.ADM_DeleteDetailTipePasienKomisiObat((short)model.Id, x.JenisPasienID);
                            }
                            foreach (var x in model.DetailTipePasien)
                            {
                                var _d = d_TipePasien.FirstOrDefault(y => y.JenisPasienID == x.Id);
                                if (_d == null)
                                {
                                    // new
                                    s.ADM_InsertDetailTipePasienKomisiObat(
                                        (short)model.Id,
                                        x.Id,
                                        x.Komisi
                                    );
                                }
                                else
                                {
                                    // edit
                                    s.ADM_UpdateDetailTipePasienKomisiObat(
                                        (short)model.Id,
                                        x.Id,
                                        x.Komisi
                                    );
                                }
                            }
                            #endregion

                            #region Obat
                            var d_Obat = s.ADM_GetDetailKomisiObat().Where(x => x.DOkterID == model.Id).ToList();
                            foreach (var x in d_Obat)
                            {
                                var _d = model.DetailObat.FirstOrDefault(y => y.Id == x.Barang_ID);
                                //delete
                                if (_d == null) s.ADM_DeleteDetailObatKomisiObat((short)model.Id, x.Barang_ID);
                            }
                            foreach (var x in model.DetailObat)
                            {
                                var _d = d_Obat.FirstOrDefault(y => y.Barang_ID == x.Id);
                                if (_d == null)
                                {
                                    // new
                                    s.ADM_InsertDetailObatKomisiObat(
                                        x.Id,
                                        model.Id
                                    );
                                }
                            }
                            #endregion

                            #region THTDokter
                            var d_THTDokter = s.ADM_GetDetailTHTDokterKomisiObat.Where(x => x.DokterID == model.Id).ToList();
                            foreach (var x in d_THTDokter)
                            {
                                var _d = model.DetailTHTDokter.FirstOrDefault(y => y.Id == x.JenisPasienID);
                                //delete
                                if (_d == null) s.ADM_DeleteDetailTHTDokterKomisiObat((short)model.Id, x.JenisPasienID);
                            }
                            foreach (var x in model.DetailTHTDokter)
                            {
                                var _d = d_THTDokter.FirstOrDefault(y => y.JenisPasienID == x.Id);
                                if (_d == null)
                                {
                                    // new
                                    s.ADM_InsertDetailTHTDokterKomisiObat(
                                        (short)model.Id,
                                        x.Id,
                                        x.THT
                                    );
                                }
                                else
                                {
                                    // edit
                                    s.ADM_UpdateDetailTHTDokterKomisiObat(
                                        (short)model.Id,
                                        x.Id,
                                        x.THT
                                    );
                                }
                            }
                            #endregion
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"VendorKomisiObat-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(int id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetKomisiObat.FirstOrDefault(x => x.Supplier_ID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d_TipePasien = s.ADM_GetDetailTipePasienKomisiObat.Where(x => x.DokterID == m.Supplier_ID).ToList();
                    var d_Obat = s.ADM_GetDetailKomisiObat().Where(x => x.DOkterID == m.Supplier_ID).ToList();
                    var d_THTDokter = s.ADM_GetDetailTHTDokterKomisiObat.Where(x => x.DokterID == m.Supplier_ID).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Id = m.Supplier_ID,
                            Kode = m.Kode_Supplier,
                            Nama = m.Nama_Supplier,
                            Alamat = m.Alamat_1,
                            TglMulaiTHT = m.TglMulaiTHT?.ToString("yyyy-MM-dd"),
                            SelesaiTHT = m.SelesaiTHT ?? false,
                            TglSelesaiTHT = m.TglSelesaiTHT?.ToString("yyyy-MM-dd"),
                        },
                        DetailTipePasien = d_TipePasien.ConvertAll(x => new
                        {
                            Id = x.JenisPasienID,
                            Komisi = x.Komisi,
                        }),
                        DetailObat = d_Obat.ConvertAll(x => new
                        {
                            Id = x.Barang_ID,
                            Kode = x.Kode_Barang,
                            Nama = x.Deskripsi,
                        }),
                        DetailTHTDokter = d_THTDokter.ConvertAll(x => new
                        {
                            Id = x.JenisPasienID,
                            THT = x.NilaiTHT,
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P

        [HttpPost]
        public string ListLookupObat(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_GetDetailObatKomisiObat.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.Kode_Barang.Contains(x.Value) ||
                                y.Nama_Barang.Contains(x.Value));
                        else if (x.Key == "Kategori" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y => y.Nama_Kategori == x.Value);
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Id = x.Barang_ID,
                        Kode = x.Kode_Barang,
                        Nama = x.Nama_Barang,
                        Kategori = x.Nama_Kategori,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}