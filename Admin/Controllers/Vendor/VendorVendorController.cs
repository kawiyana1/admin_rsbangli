﻿using Admin.Entities.SIM;
using Admin.Helper;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;
using Admin.Models.Vendor;
using static Admin.Models.Vendor.VendorVendorModel;

namespace Admin.Controllers.Vendor
{
    [Authorize]
    public class VendorVendorController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_ListVendor.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {

                            proses = proses.Where(y => y.Nama_Supplier.Contains(x.Value) ||
                                y.Alamat_1.Contains(x.Value)
                                );
                        }

                        if (x.Key == "Spesialis" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y => y.Spesialis_SpesialisName.Contains(x.Value)
                                );
                        }

                        if (x.Key == "Kategori" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y => y.KodeKategoriVendor.Contains(x.Value)
                                );
                        }


                    }


                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "DESC" : "ASC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Supplier_ID = x.Supplier_ID,
                        Nama_Supplier = x.Nama_Supplier,
                        Kode_Supplier = x.Kode_Supplier,
                        SpesialisID = x.SpesialisID,
                        Spesialis_SpesialisName = x.Spesialis_SpesialisName,
                        Alamat_1 = x.Alamat_1,
                        No_Telepon_1 = x.No_Telepon_1,
                        KatVendor_Kategori_Name = x.KatVendor_Kategori_Name,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, VendorVendorModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var id = 0;
                        var userid = User.Identity.GetUserId();
                        if (_process == "CREATE")
                        {
                            if (model.DetailKeluarga == null) model.DetailKeluarga = new List<VendorKeluargaDetailModel>();
                            if (model.DetailHobby == null) model.DetailHobby = new List<VendorHobbyDetailModel>();
                            id = s.ADM_InsertVendor(
                                model.Nama_Supplier,
                                model.Alamat,
                                model.No_Telepon,
                                model.DokterTetap,
                                model.PersonalID,
                                model.KategoriID,
                                model.SubSpesialisID,
                                model.Aktif,
                                model.SpesialisID,
                                model.THT,
                                model.HonorDefault,
                                model.Pajak,
                                model.KomisiObat,
                                model.TglLahir,
                                model.KonsultanSHHC,
                                model.HonorSudahTermasukPajak,
                                model.Password,
                                model.UserID,
                                model.SaldoAwal,
                                model.TglSaldoAwal,
                                model.PembayaranHutang,
                                model.NPWP,
                                model.KelompokHonor,
                                model.NoKTP,
                                model.TglBerlakuKTP,
                                model.JenisIjin,
                                model.NomorIjin,
                                model.TglTerbitIjin,
                                model.TglBerlakuIjin
                            );
                            foreach (var m in model.DetailKeluarga)
                            {
                                s.ADM_InsertKeluargaVendor(
                                    id,
                                    m.Nama,
                                    m.Gender,
                                    m.Hubungan,
                                    m.TglLahir);
                            }

                            foreach (var m in model.DetailHobby)
                            {
                                s.ADM_InsertHobbyVendor(
                                    id,
                                    m.Hobby,
                                    m.Keterangan
                                   );
                            }
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            id = model.Supplier_ID;
                            if (model.DetailKeluarga == null) model.DetailKeluarga = new List<VendorKeluargaDetailModel>();
                            if (model.DetailHobby == null) model.DetailHobby = new List<VendorHobbyDetailModel>();
                            s.ADM_UpdateVendor(
                                model.Kode_Supplier,
                                model.Nama_Supplier,
                                model.Alamat,
                                model.No_Telepon,
                                model.DokterTetap,
                                model.PersonalID,
                                model.KategoriID,
                                model.SubSpesialisID,
                                model.Aktif,
                                model.SpesialisID,
                                model.THT,
                                model.HonorDefault,
                                model.Pajak,
                                model.KomisiObat,
                                model.TglLahir,
                                model.KonsultanSHHC,
                                model.HonorSudahTermasukPajak,
                                model.Password,
                                model.UserID,
                                model.SaldoAwal,
                                model.TglSaldoAwal,
                                model.PembayaranHutang,
                                model.NPWP,
                                model.KelompokHonor,
                                model.NoKTP,
                                model.TglBerlakuKTP,
                                model.JenisIjin,
                                model.NomorIjin,
                                model.TglTerbitIjin,
                                model.TglBerlakuIjin,
                                model.Supplier_ID
                            );

                            var dKeluarga = s.ADM_GetKeluargaVendor.Where(x => x.SupplierID == id).ToList();
                            var dHobby = s.ADM_GetHobbyVendor.Where(x => x.Supplier_ID == id).ToList();
                            foreach (var x in dKeluarga)
                            {
                                var _d = model.DetailKeluarga.FirstOrDefault(y => y.Nama == x.Nama);
                                //delete
                                if (_d == null) s.ADM_DeleteHobbyVendor(id, x.Nama);
                            }
                            foreach (var x in dHobby)
                            {
                                var _d = model.DetailHobby.FirstOrDefault(y => y.Hobby == x.Hobby);
                                //delete
                                if (_d == null) s.ADM_DeleteHobbyVendor(id, x.Hobby);
                            }
                            foreach (var x in model.DetailKeluarga)
                            {
                                var _d = dKeluarga.FirstOrDefault(y => y.Nama == x.Nama);
                                if (_d == null)
                                {
                                    // new
                                    s.ADM_InsertKeluargaVendor(id, x.Nama, x.Gender, x.Hubungan, x.TglLahir);
                                }
                                else
                                {
                                    // edit
                                    s.ADM_UpdateKeluargaVendor(id, x.Nama, x.Gender, x.Hubungan, x.TglLahir);
                                }

                            }

                            foreach (var x in model.DetailHobby)
                            {
                                var _d = dHobby.FirstOrDefault(y => y.Hobby == x.Hobby);
                                if (_d == null)
                                {
                                    // new
                                    s.ADM_InsertHobbyVendor(id, x.Hobby, x.Keterangan);
                                }
                                else
                                {
                                    // edit
                                    s.ADM_UpdateHobbyVendor(id, x.Hobby, x.Keterangan);
                                }

                            }
                            s.SaveChanges();
                        }
                        else if (_process == "DELETE")
                        {
                            id = model.Supplier_ID;
                            s.ADM_DeleteVendor(id.ToString());
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SetupVendor-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
            return HConvert.Success();
        }

        [HttpPost]
        public string Detail(int id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_ListVendor.FirstOrDefault(x => x.Supplier_ID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var dKeluarga = s.ADM_GetKeluargaVendor.Where(x => x.SupplierID == m.Supplier_ID).OrderBy(x => x.Nama).ToList();
                    var dHobby = s.ADM_GetHobbyVendor.Where(x => x.Supplier_ID == m.Supplier_ID).OrderBy(x => x.Hobby).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Kode_Supplier = m.Kode_Supplier,
                            Nama_Supplier = m.Nama_Supplier,
                            Alamat = m.Alamat_1,
                            No_Telepon = m.No_Telepon_1,
                            KategoriID = m.KodeKategoriVendor,
                            Kategori_Name = m.KatVendor_Kategori_Name,
                            SpesialisID = m.SpesialisID,
                            SpesialisName = m.Spesialis_SpesialisName,
                            SubSpesialisID = m.SubSpesialisID,
                            SubSpesialisName = m.SubSpesialis_SubSpesialisName,
                            TglLahir = m.TglLahir == null ? "" : m.TglLahir.Value.ToString("yyyy-MM-dd"),
                            Password = m.Password ?? "",
                            DokterTetap = m.DokterTetap,
                            KonsultanSHHC = m.KonsultanSHHC,
                            PersonalID = m.IDPersonal,
                            NPWP = m.No_NPWP ?? "",
                            PembayaranHutang = m.CaraPembayaranHutang ?? "",
                            KelompokHonor = m.KelompokHonor ?? "",
                            HonorDefault = (m.HonorDefault ?? 0),
                            THT = m.THT == null ? 0 : m.THT,
                            Pajak = m.Pajak ?? 0,
                            KomisiObat = m.KomisiDefault ?? 0,
                            SaldoAwal = m.SaldoAwalHonor,
                            TglSaldoAwal = m.TglSaldoAwalHonor == null ? "" : m.TglSaldoAwalHonor.Value.ToString("yyyy-MM-dd"),
                            UserID = m.UserID ?? 0,
                            Aktif = m.Active,
                            HonorSudahTermasukPajak = m.IncludePajak,
                            NoKTP = m.NoKTP ?? "",
                            TglBerlakuKTP = m.TglBerlakuKTP == null ? "" : m.TglBerlakuKTP.Value.ToString("yyyy-MM-dd"),
                            JenisIjin = m.JenisIjin ?? "",
                            NomorIjin = m.NomorIjin ?? "",
                            Supplier_ID = m.Supplier_ID,
                            TglBerlakuIjin = m.TglBerlakuIjin == null ? "" : m.TglBerlakuIjin.Value.ToString("yyyy-MM-dd"),
                            TglTerbitIjin = m.TglTerbitIjin == null ? "" : m.TglTerbitIjin.Value.ToString("yyyy-MM-dd"),
                        },
                        DetailKeluarga = dKeluarga.ConvertAll(x => new
                        {
                            Nama = x.Nama,
                            Gender = x.Gender,
                            Hubungan = x.Hubungan,
                            TglLahir = x.TglLahir,
                        }),
                        DetailHobby = dHobby.ConvertAll(x => new
                        {
                            Hobby = x.Hobby,
                            Keterangan = x.Keterangan
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - R E K E N I N G

        [HttpPost]
        public string ListLookupPersonal(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_GetPersonalVendor.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.PersonalID.Contains(x.Value) ||
                                y.Nama.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        PersonalID = x.PersonalID,
                        Nama = x.Nama,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P  D U P L I C A T E

        [HttpPost]
        public string SaveDuplicate(string _process, VendorVendorModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var id = 0;
                        var userid = User.Identity.GetUserId();
                        if (_process == "EDIT")
                        {
                            if (model.DetailKeluarga == null) model.DetailKeluarga = new List<VendorKeluargaDetailModel>();
                            if (model.DetailHobby == null) model.DetailHobby = new List<VendorHobbyDetailModel>();
                            id = s.ADM_InsertVendor(
                                model.Nama_Supplier,
                                model.Alamat,
                                model.No_Telepon,
                                model.DokterTetap,
                                model.PersonalID,
                                model.KategoriID,
                                model.SubSpesialisID,
                                model.Aktif,
                                model.SpesialisID,
                                model.THT,
                                model.HonorDefault,
                                model.Pajak,
                                model.KomisiObat,
                                model.TglLahir,
                                model.KonsultanSHHC,
                                model.HonorSudahTermasukPajak,
                                model.Password,
                                model.UserID,
                                model.SaldoAwal,
                                model.TglSaldoAwal,
                                model.PembayaranHutang,
                                model.NPWP,
                                model.KelompokHonor,
                                model.NoKTP,
                                model.TglBerlakuKTP,
                                model.JenisIjin,
                                model.NomorIjin,
                                model.TglTerbitIjin,
                                model.TglBerlakuIjin
                            );
                            foreach (var m in model.DetailKeluarga)
                            {
                                s.ADM_InsertKeluargaVendor(
                                    id,
                                    m.Nama,
                                    m.Gender,
                                    m.Hubungan,
                                    m.TglLahir);
                            }

                            foreach (var m in model.DetailHobby)
                            {
                                s.ADM_InsertHobbyVendor(
                                    id,
                                    m.Hobby,
                                    m.Keterangan
                                   );
                            }
                            s.SaveChanges();
                        }
                        else if (_process == "DELETE")
                        {
                            id = model.Supplier_ID;
                            s.ADM_DeleteVendor(id.ToString());
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SetupVendor-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
            return HConvert.Success();
        }

        [HttpPost]
        public string DetailDuplicate(int id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_ListVendor.FirstOrDefault(x => x.Supplier_ID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var dKeluarga = s.ADM_GetKeluargaVendor.Where(x => x.SupplierID == m.Supplier_ID).OrderBy(x => x.Nama).ToList();
                    var dHobby = s.ADM_GetHobbyVendor.Where(x => x.Supplier_ID == m.Supplier_ID).OrderBy(x => x.Hobby).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Kode_Supplier = m.Kode_Supplier,
                            Nama_Supplier = m.Nama_Supplier,
                            Alamat = m.Alamat_1,
                            No_Telepon = m.No_Telepon_1,
                            KategoriID = m.KodeKategoriVendor,
                            Kategori_Name = m.KatVendor_Kategori_Name,
                            SpesialisID = m.SpesialisID,
                            SpesialisName = m.Spesialis_SpesialisName,
                            SubSpesialisID = m.SubSpesialisID,
                            SubSpesialisName = m.SubSpesialis_SubSpesialisName,
                            TglLahir = m.TglLahir == null ? "" : m.TglLahir.Value.ToString("yyyy-MM-dd"),
                            Password = m.Password ?? "",
                            DokterTetap = m.DokterTetap,
                            KonsultanSHHC = m.KonsultanSHHC,
                            PersonalID = m.IDPersonal,
                            NPWP = m.No_NPWP ?? "",
                            PembayaranHutang = m.CaraPembayaranHutang ?? "",
                            KelompokHonor = m.KelompokHonor ?? "",
                            HonorDefault = (m.HonorDefault ?? 0),
                            THT = m.THT == null ? 0 : m.THT,
                            Pajak = m.Pajak ?? 0,
                            KomisiObat = m.KomisiDefault ?? 0,
                            SaldoAwal = m.SaldoAwalHonor,
                            TglSaldoAwal = m.TglSaldoAwalHonor == null ? "" : m.TglSaldoAwalHonor.Value.ToString("yyyy-MM-dd"),
                            UserID = m.UserID ?? 0,
                            Aktif = m.Active,
                            HonorSudahTermasukPajak = m.IncludePajak,
                            NoKTP = m.NoKTP ?? "",
                            TglBerlakuKTP = m.TglBerlakuKTP == null ? "" : m.TglBerlakuKTP.Value.ToString("yyyy-MM-dd"),
                            JenisIjin = m.JenisIjin ?? "",
                            NomorIjin = m.NomorIjin ?? "",
                            Supplier_ID = m.Supplier_ID,
                            TglBerlakuIjin = m.TglBerlakuIjin == null ? "" : m.TglBerlakuIjin.Value.ToString("yyyy-MM-dd"),
                            TglTerbitIjin = m.TglTerbitIjin == null ? "" : m.TglTerbitIjin.Value.ToString("yyyy-MM-dd"),
                        },
                        DetailKeluarga = dKeluarga.ConvertAll(x => new
                        {
                            Nama = x.Nama,
                            Gender = x.Gender,
                            Hubungan = x.Hubungan,
                            TglLahir = x.TglLahir,
                        }),
                        DetailHobby = dHobby.ConvertAll(x => new
                        {
                            Hobby = x.Hobby,
                            Keterangan = x.Keterangan
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}