﻿using Admin.Entities.SIM;
using Admin.Helper;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using System.Linq.Dynamic;
using Admin.Models.Vendor;
using System.Security.Cryptography.X509Certificates;
using System.Web.ModelBinding;

namespace Admin.Controllers.Vendor
{
    public class VendorHargaJasaLuarController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_ListHargaJasaLuar.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>  y.Nama_Supplier.Contains(x.Value));

                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        SupplierID = x.Supplier_ID,
                        Nama_Supplier = x.Nama_Supplier,
                        Alamat_1 = x.Alamat_1 ?? "",
                        No_Telepon_1 = x.No_Telepon_1 ?? "",
                    }); 
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, VendorHargaJasaLuarModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        var HargaJasaLuar = "";
                        if (_process == "EDIT")
                        {


                            var d = s.ADM_GetHargaJasaLuarDetail(model.SupplierID).ToList();
                            foreach (var x in d)
                            {
                                var _d = model.Detail.FirstOrDefault(y => y.JasaID == x.JasaID);
                                //delete
                                if (_d == null) s.ADM_DeleteHargaJasLuar(model.JasaID);
                            }
                            foreach (var x in model.Detail)
                            {
                                var _d = d.FirstOrDefault(y => y.JasaID == x.JasaID);
                                if (_d == null)
                                {
                                    // new
                                    s.ADM_InsertHargaJasaLuar(x.JasaID, model.SupplierID, x.Harga, x.HargaLama, x.TglBerlaku, x.TipePasien, x.PasienKTP, x.HargaPokok, x.HargaPokok2);
                                }
                                else
                                {
                                    // edit
                                    s.ADM_UpdateHargaJasaLuar(x.JasaID, model.SupplierID, x.Harga, x.HargaLama, x.TglBerlaku, x.TipePasien, x.PasienKTP, x.HargaPokok, x.HargaPokok2);
                                }

                            }
                            s.SaveChanges();
                        }
                        else if (_process == "DELETE")
                        {
                            HargaJasaLuar = model.JasaID;
                            s.ADM_DeleteHargaJasLuar(model.JasaID);
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SetupHargaJasaLuar-{_process}; id:{HargaJasaLuar};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
            return HConvert.Success();
        }

        [HttpPost]
        public string Detail(short id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_ListHargaJasaLuar.FirstOrDefault(x => x.Supplier_ID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.ADM_GetHargaJasaLuarDetail(m.Supplier_ID).Where(x => x.SupplierID == m.Supplier_ID).OrderBy(x => x.JasaName).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            SupplierID = m.Supplier_ID,
                            Nama_Supplier = m.Nama_Supplier,
                            Alamat_1 = m.Alamat_1,
                            No_Telepon_1 = m.No_Telepon_1,
                        },
                        Detail = d.ConvertAll(x => new {
                            JasaID = x.JasaID,
                            JasaName = x.JasaName,
                            TipePasien = x.TipePasien,
                            PasienKTP = x.PasienKTP,
                            Harga = x.Harga,
                            HargaPokok = x.HargaPokok,
                            HargaPokok2 = x.HargaPokok2,
                            HargaLama = x.HargaLama,
                            TglBerlaku = x.TglBerlaku?.ToString("yyyy-MM-dd"),
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - R E K E N I N G

        [HttpPost]
        public string ListLookupJasa(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_GetJasa.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.JasaID.Contains(x.Value) ||
                                y.JasaName.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        JasaID = x.JasaID,
                        JasaName = x.JasaName,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}