﻿using Admin.Entities.SIM;
using Admin.Helper;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;
using Admin.Models.Vendor;

namespace Admin.Controllers.Vendor
{
    public class VendorAsuransiController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_ListAsuransi.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y => y.Asuransi_ID.Contains(x.Value) ||
                                y.Asuransi_Name.Contains(x.Value) ||
                                y.MarkUp_Prosen.ToString().Contains(x.Value)
                                );
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        Asuransi_ID = x.Asuransi_ID,
                        Asuransi_Name = x.Asuransi_Name,
                        MarkUp_Prosen = x.MarkUp_Prosen
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, VendorAsuransiModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        var asuransiID = "";
                        if (_process == "CREATE")
                        {
                            if (model.Detail == null) model.Detail = new List<VendorAsuransiDetailModel>();
                            asuransiID = s.ADM_InsertAsuransi(
                                model.Asuransi_Name,
                                model.MarkUp_Prosen,
                                model.TermasukObat
                            ).FirstOrDefault();
                            foreach (var m in model.Detail)
                            {
                                s.ADM_InsertAsuransiDetail(asuransiID, m.JasaID);
                            }
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            if (model.Detail == null) model.Detail = new List<VendorAsuransiDetailModel>();
                            asuransiID = model.Asuransi_ID;
                            s.ADM_UpdateAsuransi(
                                model.Asuransi_ID,
                                model.Asuransi_Name,
                                model.MarkUp_Prosen,
                                model.TermasukObat
                            );

                            var d = s.ADM_GetAsuransiDetail.Where(x => x.Asuransi_ID == model.Asuransi_ID).ToList();
                            foreach (var x in d)
                            {
                                var _d = model.Detail.FirstOrDefault(y => y.JasaID == x.JasaID);
                                //delete
                                if (_d == null) s.ADM_DeleteAsuransiDetail(model.Asuransi_ID, x.JasaID);
                            }
                            foreach (var x in model.Detail)
                            {
                                var _d = d.FirstOrDefault(y => y.JasaID == x.JasaID);
                                if (_d == null)
                                {
                                    // new
                                    s.ADM_InsertAsuransiDetail(model.Asuransi_ID, x.JasaID);
                                }

                            }
                            s.SaveChanges();
                        }
                        else if (_process == "DELETE")
                        {
                            asuransiID = model.Asuransi_ID;
                            s.ADM_DeleteAsuransi(model.Asuransi_ID);
                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SetupAsuransi-{_process}; id:{asuransiID};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
            return HConvert.Success();
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetAsuransi.FirstOrDefault(x => x.Asuransi_ID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.ADM_GetAsuransiDetail.Where(x => x.Asuransi_ID == m.Asuransi_ID).OrderBy(x => x.JasaName).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Asuransi_ID = m.Asuransi_ID,
                            Asuransi_Name = m.Asuransi_Name,
                            MarkUp_Prosen = m.MarkUp_Prosen,
                            TermasukObat = m.TermasukObat,
                        },
                        Detail = d.ConvertAll(x => new {
                            JasaID = x.JasaID,
                            JasaName = x.JasaName,
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K U P - R E K E N I N G

        [HttpPost]
        public string ListLookupJasa(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.ADM_GetJasaAsuransi.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                                y.JasaID.Contains(x.Value) ||
                                y.Deskripsi.Contains(x.Value));
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        JasaID = x.JasaID,
                        Deskripsi = x.Deskripsi,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}