﻿using Admin.Entities.SIM;
using Admin.Helper;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;
using Admin.Models.Report;
using System.Threading.Tasks;
using System.Security.Claims;

namespace Admin.Controllers.Report
{
    public class SReportController : Controller
    {
        // GET: SReport
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.sReport.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y =>
                            y.Baris1.Contains(x.Value) ||
                            y.Baris2.Contains(x.Value));

                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        ID = x.ID,
                        Baris1 = x.Baris1 == null ? "" : x.Baris1,
                        Baris2 = x.Baris2 == null ? "" : x.Baris2,
                        Baris3 = x.Baris3 == null ? "" : x.Baris3,
                        Baris4 = x.Baris4 == null ? "" : x.Baris4,
                        Baris5 = x.Baris5 == null ? "" : x.Baris5,
                        logo = x.logo,
                    }); 
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, SReportModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        
                        if (_process == "EDIT")
                        {
                            var m = s.sReport.FirstOrDefault(x => x.ID == model.ID);

                            m.ID = model.ID;
                            m.Baris1 = model.Baris1;
                            m.Baris2 = model.Baris2;
                            m.Baris3 = model.Baris3;
                            m.Baris4 = model.Baris4;
                            m.Baris5 = model.Baris5;
                            m.logo = HConvert.GetByteArrayFromImage(model.logo);
                            s.SaveChanges();
                        }

                        dbContextTransaction.Commit();
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
            return HConvert.Success();
        }

        [HttpPost]
        public string Detail(int id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var userid = User.Identity.GetUserId();

                    var m = s.sReport.FirstOrDefault(x => x.ID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            ID = m.ID,
                            Baris1 = m.Baris1,
                            Baris2 = m.Baris2,
                            Baris3 = m.Baris3,
                            Baris4 = m.Baris4,
                            Baris5 = m.Baris5,
                            logo = Convert.ToBase64String(m.logo),

                        }
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion


        //#region ===== G A M B A R
        //[HttpPost]
        //[ActionName("ActionPertanyaan")]
        //public async Task<JsonResult> Post_Pertanyaan(sReport model)
        //{

        //    var userid = User.FindFirstValue(ClaimTypes.NameIdentifier);
        //    using (var s = new MahasConnection(Config.GetConnectionString("FAQConnection")))
        //    {
        //        try
        //        {
        //            s.();
        //            model.UserID = userid;
        //            model.IdUnit = Config.GetSection("FAQ_Settings")["UnitID"];
        //            model.IdAplikasi = Config.GetSection("FAQ_Settings")["AplikasiID"];
        //            model.TanggalBuat = DateTime.Now;
        //            model.Selesai = false;
        //            model.Admin = User.IsInRole("Admin");

        //            if (model.Gambar1_View != null)
        //            {
        //                model.Gambar1 = MahasConverter.GetByteArrayFromImage(model.Gambar1_View);
        //            }
        //            if (model.Gambar2_View != null)
        //            {
        //                model.Gambar2 = MahasConverter.GetByteArrayFromImage(model.Gambar2_View);
        //            }
        //            if (model.Gambar3_View != null)
        //            {
        //                model.Gambar3 = MahasConverter.GetByteArrayFromImage(model.Gambar3_View);
        //            }
        //            model.IdPertanyaan = await s.Insert(model, true);
        //            s.Transaction.Commit();
        //        }
        //        catch (Exception ex)
        //        {
        //            return Json(new
        //            {
        //                Success = false,
        //                ex.Message
        //            });
        //        }
        //    }

        //    return Json(new
        //    {
        //        Success = true,
        //        Data = model
        //    });
        //}
        //#endregion



    }
}