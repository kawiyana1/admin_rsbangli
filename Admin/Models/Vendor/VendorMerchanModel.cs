﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Vendor
{
    public class VendorMerchanModel
    {
        public string ID { get; set; }
        public string NamaBank { get; set; }
        public int Akun_ID { get; set; }
        public int Akun_Akun_No { get; set; }
        public string Akun_Akun_Name { get; set; }
        public Nullable<double> AddCharge_Debet { get; set; }
        public Nullable<double> AddCharge_Kredit { get; set; }
        public Nullable<double> Diskon { get; set; }
    }
}