﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Vendor
{
    public class VendorKomisiObatModel
    {
        public int Id { get; set; }
        public DateTime? TglMulaiTHT { get; set; }
        public bool SelesaiTHT { get; set; }
        public DateTime? TglSelesaiTHT { get; set; }
        public List<VendorKomisiObatDetailTipePasienModel> DetailTipePasien { get; set; }
        public List<VendorKomisiObatDetailObatModel> DetailObat { get; set; }
        public List<VendorKomisiObatDetailTHTDokterModel> DetailTHTDokter { get; set; }
    }

    public class VendorKomisiObatDetailTipePasienModel
    {
        public int Id { get; set; }
        public double Komisi { get; set; }
    }
    public class VendorKomisiObatDetailObatModel
    {
        public int Id { get; set; }
    }
    public class VendorKomisiObatDetailTHTDokterModel 
    {
        public int Id { get; set; }
        public double THT { get; set; }
    }
}