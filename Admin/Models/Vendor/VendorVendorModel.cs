﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Vendor
{
    public class VendorVendorModel
    {
        public string Kode_Supplier { get; set; }
        public string Nama_Supplier { get; set; }
        public string Alamat { get; set; }
        public string No_Telepon { get; set; }
        public string KategoriID { get; set; }
        public string Kategori_Name { get; set; }
        public string SpesialisID { get; set; }
        public string SpesialisName { get; set; }
        public string SubSpesialisID { get; set; }
        public string SubSpesialisName { get; set; }
        public string PersonalID { get; set; }
        public string PersonalName { get; set; }
        public string NPWP { get; set; }
        public string PembayaranHutang { get; set; }
        public string KelompokHonor { get; set; }
        public Nullable<System.DateTime> TglLahir { get; set; }
        public string Password { get; set; }
        public bool KonsultanSHHC { get; set; }
        public Nullable<bool> DokterTetap { get; set; }
        public Nullable<double> HonorDefault { get; set; }
        public Nullable<double> THT { get; set; }
        public Nullable<double> Pajak { get; set; }
        public Nullable<double> KomisiObat { get; set; }
        public short Supplier_ID { get; set; }
        public string NoKTP { get; set; }
        public decimal SaldoAwal { get; set; }
        public Nullable<System.DateTime> TglSaldoAwal { get; set; }
        public string UserID { get; set; }
        public Nullable<System.DateTime> TglBerlakuKTP { get; set; }
        public bool Aktif { get; set; }
        public bool HonorSudahTermasukPajak { get; set; }
        public string JenisIjin { get; set; }
        public string NomorIjin { get; set; }
        public Nullable<System.DateTime> TglTerbitIjin { get; set; }
        public Nullable<System.DateTime> TglBerlakuIjin { get; set; }

        public List<VendorKeluargaDetailModel> DetailKeluarga { get; set; }
        public List<VendorHobbyDetailModel> DetailHobby { get; set; }

        public class VendorKeluargaDetailModel
        {
            public short SupplierID { get; set; }
            public string Nama { get; set; }
            public string Gender { get; set; }
            public string Hubungan { get; set; }
            public System.DateTime TglLahir { get; set; }
        }

        public class VendorHobbyDetailModel
        {
            public short Supplier_ID { get; set; }
            public string Hobby { get; set; }
            public string Keterangan { get; set; }
        }
    }
}