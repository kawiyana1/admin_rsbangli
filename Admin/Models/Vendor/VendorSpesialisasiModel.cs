﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Vendor
{
    public class VendorSpesialisasiModel
    {
        public string SpesialisID { get; set; }
        public string SpesialisName { get; set; }
    }
}