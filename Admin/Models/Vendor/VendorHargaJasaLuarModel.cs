﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Vendor
{
    
    public class VendorHargaJasaLuarModel
    {
        public string JasaID { get; set; }
        public short SupplierID { get; set; }
        public short Supplier_ID { get; set; }
        public string JasaName { get; set; }
        public decimal Harga { get; set; }
        public decimal HargaLama { get; set; }
        public decimal HargaPokok { get; set; }
        public decimal HargaPokok2 { get; set; }
        public DateTime TglBerlaku { get; set; }
        public string TipePasien { get; set; }
        public bool PasienKTP { get; set; }

        public List<VendorHargaJasaLuarDetailModel> Detail { get; set; }
    }

    public class VendorHargaJasaLuarDetailModel
    {
        public int? SupplierID { get; set; }
        public short Supplier_ID { get; set; }
        public string JasaID { get; set; }
        public string JasaName { get; set; }
        public decimal Harga { get; set; }
        public decimal HargaLama { get; set; }
        public decimal HargaPokok { get; set; }
        public decimal HargaPokok2 { get; set; }
        public DateTime TglBerlaku { get; set; }
        public string TipePasien { get; set; }
        public bool PasienKTP { get; set; }
    }
}