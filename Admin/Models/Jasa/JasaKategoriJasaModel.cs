﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Jasa
{
    public class JasaKategoriJasaModel
    {
        public string KategoriJasaName { get; set; }
        public int GroupJasa { get; set; }
        public string NamaInternational { get; set; }
        public string KategoriJasaID { get; set; }
       
    }
}