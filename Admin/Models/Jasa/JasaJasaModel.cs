﻿using CrystalDecisions.ReportSource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Jasa
{
    public class JasaJasaModel
    {
        public string Id { get; set; }
        public string Nama { get; set; }
        public string NamaInternasional { get; set; }
        public byte GroupJasa { get; set; }
        public string KategoriJasa { get; set; }
        public string KategoriPlafon { get; set; }
        public string KeteranganDiInvoice { get; set; }
        public bool Aktif { get; set; }
        public bool MengunakanAlatPenunjang { get; set; }
        public bool JasaPaket { get; set; }
        public bool TarifManual { get; set; }
        public string MappingPemakaianAlat { get; set; }
        public string KelompokPostinganBy { get; set; }
        public string SumberInsentif { get; set; }
        public bool HariRawatInap { get; set; }
        public bool MCU { get; set; }
        public bool AutoSystem { get; set; }
        public bool KSO { get; set; }
        public bool VariableKategoriOperasi { get; set; }
        public bool VariableCito { get; set; }
        public bool HarusDenganDokterPetugas { get; set; }
        public string Poliklinik { get; set; }
        public double? CostRS { get; set; }
        public decimal? CostRSRp { get; set; }
        public List<JasaJasaDetailHargaUmumModel> DetailHargaUmum { get; set; }
        public List<JasaJasaDetailBHPModel> DetailBHP { get; set; }
        public List<JasaJasaDetailSectionModel> DetailSection { get; set; }
        public List<JasaJasaDetailDataLABModel> DetailDataLAB { get; set; }
    }

    public class JasaJasaDetailHargaUmumModel
    {
        public int Id { get; set; }
        public string Kelas { get; set; }
        public int TipePasien { get; set; }
        public string Spesialis { get; set; }
        public bool SubSpesialis { get; set; }
        public string Dokter { get; set; }
        public int KatOperasi { get; set; }
        public bool Cito { get; set; }
        public bool KTP { get; set; }
        public string Lokasi { get; set; }
        public decimal HargaLama { get; set; }
        public decimal HargaBaru { get; set; }
        public decimal HargaHCLama { get; set; }
        public decimal HargaHC { get; set; }
        public double DiscHC { get; set; }
        public DateTime? TglBerlaku { get; set; }
        public decimal HargaBPJS { get; set; }
        public decimal HargaBPJSLama { get; set; }
        public DateTime? TglBerlakuBPJS { get; set; }
        public decimal InsentifDetail { get; set; }
        public decimal InsentifKomponen { get; set; }
        public List<JasaJasaDetailHargaUmumDetailModel> Detail { get; set; }
    }

    public class JasaJasaDetailHargaUmumDetailModel 
    {
        public string Id { get; set; }
        public string Nama { get; set; }
        public double Qty { get; set; }
        public bool UsePersen { get; set; }
        public double Persen { get; set; }
        public double Insentif { get; set; }
        public bool IncInsent { get; set; }
        public decimal HargaAwal { get; set; }
        public decimal HargaLama { get; set; }
        public decimal HargaBaru { get; set; }
        public decimal HargaHCLama { get; set; }
        public decimal HargaHC { get; set; }
        public double DiscInsentifHC { get; set; }
        public string AkunHPP { get; set; }
        public string AkunLawan { get; set; }
        public decimal HargaBPJS { get; set; }
        public decimal HargaBPJSLama { get; set; }
    }

    public class JasaJasaDetailBHPModel
    {
        public string Id { get; set; }
        public double Qty { get; set; }
        public bool Ditagihkan { get; set; }
    }

    public class JasaJasaDetailSectionModel
    {
        public string Id { get; set; }
    }

    public class JasaJasaDetailDataLABModel
    {
        public string Id { get; set; }
    }
}