﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Jasa
{
    public class JasaKategoriOperasiModel
    {
        public int KategoriID { get; set; }
        public string KategoriName { get; set; }
    }
}