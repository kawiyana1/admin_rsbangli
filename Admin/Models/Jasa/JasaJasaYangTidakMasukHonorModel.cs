﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Jasa
{
    public class JasaJasaYangTidakMasukHonorModel
    {
        public string JasaID { get; set; }
        public string TipeHonor { get; set; }
        public bool Opt { get; set; }
        public string TipePasien { get; set; }
        public string JasaIDLama { get; set; }
        public bool OptLama { get; set; }
        public string TipeHonorLama { get; set; }
        public string TipePasienLama { get; set; }


    }
}