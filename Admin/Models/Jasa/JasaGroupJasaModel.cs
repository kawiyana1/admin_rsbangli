﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Jasa
{
    public class JasaGroupJasaModel
    {
        public byte GroupJasaID { get; set; }
        public string GroupJasaName { get; set; }
        public string KelompokPerawatan { get; set; }
        public Nullable<bool> HonorDokter { get; set; }
        public Nullable<bool> THT { get; set; }
        public Nullable<bool> INcentive { get; set; }
        public string NamaInternational { get; set; }
        public string AkunNoRI { get; set; }
        public string AkunNoRJ { get; set; }
        public string AKunNoUGD { get; set; }
        public string AkunNoOnCall { get; set; }
        public string AkunNoMA { get; set; }

    }
}