﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Jasa
{
    public class JasaKelasModel
    {
        public string KelasID { get; set; }
        public string NamaKelas { get; set; }
        public bool Active { get; set; }
        public int Nomor { get; set; }
    }
}