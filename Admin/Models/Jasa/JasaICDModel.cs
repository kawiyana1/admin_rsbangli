﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Jasa
{
    public class JasaICDModel
    {
        public string KodeICD { get; set; }
        public string SectionID { get; set; }
        public string Descriptions { get; set; }
        public string DiagnosaHC { get; set; }
        public string IDGroupICD { get; set; }
    }
}