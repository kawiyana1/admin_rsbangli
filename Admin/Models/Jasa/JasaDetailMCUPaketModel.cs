﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Jasa
{
    public class JasaDetailMCUPaketModel
    {
        public string Id { get; set; }
        public List<JasaDetailMCUPaketDetailModel> Detail { get; set; }
    }

    public class JasaDetailMCUPaketDetailModel 
    {
        public string Id { get; set; }
        public string Section { get; set; }
        public decimal Harga { get; set; }
        public decimal HargaHC { get; set; }
        public string Kelompok { get; set; }
        public double Qty { get; set; }
        public List<JasaDetailMCUPaketDetailDetailModel> Detail { get; set; }
    }

    public class JasaDetailMCUPaketDetailDetailModel
    {
        public string Id { get; set; }
        public decimal Harga { get; set; }
        public decimal HargaHC { get; set; }
    }
}