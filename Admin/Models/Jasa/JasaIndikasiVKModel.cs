﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Jasa
{
    public class JasaIndikasiVKModel
    {
        public string IndikasiID { get; set; }
        public string Deskripsi { get; set; }
        public string IndikasiIDLama { get; set; }
        public string DeskripsiLama { get; set; }
    }
}