﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Jasa
{
    public class JasaRuangOKModel
    {
        public string RuangOK { get; set; }
        public string RuangOKID { get; set; }
        public string RuangOKLama { get; set; }
        public string RuangOKIDLama { get; set; }
    }
}