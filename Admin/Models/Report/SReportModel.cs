﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Admin.Models.Report
{
    public class SReportModel
    {
        public int ID { get; set; }
        public string Baris1 { get; set; }
        public string Baris2 { get; set; }
        public string Baris3 { get; set; }
        public string Baris4 { get; set; }
        public string Baris5 { get; set; }

        public HttpPostedFileBase logo { get; set; }
    }

  
}