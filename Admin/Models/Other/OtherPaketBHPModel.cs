﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Other
{
    public class OtherPaketBHPModel
    {
        public string Id { get; set; }
        public string Nama { get; set; }
        public string Section { get; set; }
        public string Ditagihkan { get; set; }
        public bool IncludeJasa { get; set; }
        public List<OtherPaketBHPDetailModel> Detail { get; set; }
    }

    public class OtherPaketBHPDetailModel 
    {
        public string Id { get; set; }
        public double Qty { get; set; }
    }
}