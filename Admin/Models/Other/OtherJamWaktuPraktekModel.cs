﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Other
{
    public class OtherJamWaktuPraktekModel
    {
        public int Id { get; set; }
        public string Nama { get; set; }
        public TimeSpan DariJam { get; set; }
        public TimeSpan SampaiJam { get; set; }
        public string Keterangan { get; set; }
    }
}