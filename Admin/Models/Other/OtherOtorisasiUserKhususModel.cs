﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Other
{
    public class OtherOtorisasiUserKhususModel
    {
        public string KriteriaOtorisasi { get; set; }
        public int User { get; set; }
    }
}