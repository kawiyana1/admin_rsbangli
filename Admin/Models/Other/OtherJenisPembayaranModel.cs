﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Other
{
    public class OtherJenisPembayaranModel
    {
        public int Id { get; set; }
        public string Nama { get; set; }
        public int NoUrut { get; set; }
        public int Rekening { get; set; }
        public bool Cash { get; set; }
        public bool Bank { get; set; }
        public bool KartuKredit { get; set; }
        public bool Jaminan { get; set; }
        public bool Other { get; set; }
        public bool KerjasamaOnly { get; set; }
        public bool HCOnly { get; set; }
        public bool AsuransiOnly { get; set; }
        public string KelompokPosting { get; set; }
    }
}