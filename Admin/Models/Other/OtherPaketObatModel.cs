﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Other
{
    public class OtherPaketObatModel
    {
        public string Id { get; set; }
        public string Nama { get; set; }
        public string Section { get; set; }
        public int Dokter { get; set; }
        public bool ObatPuyer { get; set; }
        public string Keterangan { get; set; }
        public List<OtherPaketObatDetailModel> Detail { get; set; }
    }

    public class OtherPaketObatDetailModel 
    {
        public int Id { get; set; }
        public double Qty { get; set; }
        public string AturanPakai { get; set; }
    }
}