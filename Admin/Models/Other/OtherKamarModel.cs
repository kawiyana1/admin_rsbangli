﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Other
{
    public class OtherKamarModel
    {
        public string Id { get; set; }
        public string NoKamar { get; set; }
        public string NamaKamar { get; set; }
        public string Sal { get; set; }
        public byte JmlBed { get; set; }
        public byte NoLantai { get; set; }
        public bool Tambahan { get; set; }
        public string Kelas { get; set; }
        public List<OtherKamarDetailModel> Detail { get; set; }
    }

    public class OtherKamarDetailModel 
    {
        public string Id { get; set; }
    }
}