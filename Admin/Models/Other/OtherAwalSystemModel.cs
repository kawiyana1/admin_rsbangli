﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Other
{
    public class OtherAwalSystemModel
    {
        public string Nama { get; set; }
        public string Display { get; set; }
        public string Nilai { get; set; }
        public string AkunNo { get; set; }
        public string AkunNama { get; set; }
        public string Tipe { get; set; }
    }
}