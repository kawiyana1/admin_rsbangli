﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Other
{
    public class OtherGradingHargaObatModel
    {
        public string Id_TipePelayanan { get; set; }
        public int Id_TipePasien { get; set; }
        public string Id_Kelas { get; set; }
        public string Id_KelompokGradingObat { get; set; }
        public bool Id_KTP { get; set; }
        public decimal Id_RangeHarga1 { get; set; }

        public string TipePelayanan { get; set; }
        public int TipePasien { get; set; }
        public string Kelas { get; set; }
        public string GolonganObat { get; set; }
        public string KelompokGradingObat { get; set; }
        public bool KTP { get; set; }
        public decimal? RangeHarga1 { get; set; }
        public decimal? RangeHarga2 { get; set; }
        public double? PresentaseUp { get; set; }
        public double? Diskon { get; set; }
        public double? PPN { get; set; }
        public DateTime TglMulaiBerlaku { get; set; }
    }
}