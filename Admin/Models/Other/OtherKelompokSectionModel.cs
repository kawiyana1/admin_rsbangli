﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Other
{
    public class OtherKelompokSectionModel
    {
        public int Id { get; set; }
        public int Nomor { get; set; }
        public string Nama { get; set; }
        public bool ProfitCenter { get; set; }
    }
}