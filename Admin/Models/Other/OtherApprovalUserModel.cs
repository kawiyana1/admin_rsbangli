﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Models.Other
{
    public class OtherApprovalUserModel
    {
        public string Function { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string ConfrimPassword { get; set; }
    }
}