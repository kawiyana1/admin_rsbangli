﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Admin.Entities.SIM;
using Newtonsoft.Json;

namespace Admin.Helper
{
    public static class HList
    {
        public static List<SelectListItem> ListBulan
        {
            get
            {
                return new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "Januari", Value = "1" },
                    new SelectListItem(){ Text = "Febuari", Value = "2" },
                    new SelectListItem(){ Text = "Maret", Value = "3" },
                    new SelectListItem(){ Text = "April", Value = "4" },
                    new SelectListItem(){ Text = "Mei", Value = "5" },
                    new SelectListItem(){ Text = "Juni", Value = "6" },
                    new SelectListItem(){ Text = "Juli", Value = "7" },
                    new SelectListItem(){ Text = "Agustus", Value = "8" },
                    new SelectListItem(){ Text = "September", Value = "9" },
                    new SelectListItem(){ Text = "Oktober", Value = "10" },
                    new SelectListItem(){ Text = "November", Value = "11" },
                    new SelectListItem(){ Text = "Desember", Value = "12" },
                };
            }
        }

        #region ===== J A S A

        public static string Jasa_DetailMCUPaket_Kategori
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetKategori.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.KategoriJasaID}\">{x.KategoriJasaName}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Jasa_DetailMCUPaket_Section
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetSectionMCU.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.SectionID}\">{x.SectionName}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Jasa_Jasa_KategoriPlafon
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetKategoriPlafonJasa.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.KategoriPlafon}\">{x.KategoriPlafon}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Jasa_Jasa_KeteranganDiInvoice
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetKeteranganInvoiceJasa.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Keterangan}\">{x.Keterangan}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Jasa_Jasa_MappingPemakaianAlat
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_ListAlat.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.IDAlat}\">{x.IDAlat} - {x.NamaAlat}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Jasa_Jasa_KelompokPostinganBy
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetJasaKepompokPostingan.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.A}\">{x.A}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Jasa_Jasa_SumberInsentif
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetSumberInsentifJasa.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.A}\">{x.A}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Jasa_Jasa_Poliklinik
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetJasaPoliklinik.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.A}\">{x.A}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Jasa_Jasa_Kelas
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_ListKelas.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.KelasID}\">{x.NamaKelas}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Jasa_Jasa_TipePasien
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetJenisKerjasama.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.JenisKerjasamaID}\">{x.JenisKerjasama}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Jasa_Jasa_Spesialis
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_ListSpesialisasi.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.SpesialisID}\">{x.SpesialisName}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Jasa_Jasa_KategoriOperasi
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_ListKategoriOperasi.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.KategoriID}\">{x.KategoriName}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Jasa_Jasa_Lokasi
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetLokasiJasa.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Lokasi}\">{x.Lokasi}</option>";
                    }
                    return r;
                }
            }
        }

        #endregion

        #region ===== V E N D O R
        public static string Vendor_KomisiObat_Kategori
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetLookUpBarangFilterKategori.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Nama_Kategori}\">{x.Nama_Kategori}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Vendor_KomisiObat_TipePasien
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetKomisiObatTipePasien.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.JenisKerjasamaID}\">{x.JenisKerjasama}</option>";
                    }
                    return r;
                }
            }
        }
        #endregion

        #region ===== O T H E R

        public static string Other_GradingHargaObat_List_TipePelayanan
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetTipePelayananSection.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.A}\">{x.A}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Other_GradingHargaObat_TipePelayanan
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetTipePelayananSection.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.A}\">{x.A}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Other_GradingHargaObat_TipePasien
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetTipePasienGradHargaObat.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.JenisKerjasamaID}\">{x.JenisKerjasama}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Other_GradingHargaObat_Kelas
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetKelas.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.KelasID}\">{x.NamaKelas}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Other_GradingHargaObat_GolonganObat
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetGolonganObat.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.NamaGolongan}\">{x.NamaGolongan}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Other_GradingHargaObat_KelompokGradingObat
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetKelompokGrading.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.KelompokGrading}\">{x.KelompokGrading}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Other_Section_List_UnitBisnis
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetUnitBisnisSection.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.UnitBisnisID}\">{x.UnitBisnisName}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Other_Section_TipePelayanan
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetTipePelayananSection.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.A}\">{x.A}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Other_Section_KelompokSection
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetKelompokSection.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.A}\">{x.A}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Other_Section_Poliklinik
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetPoliklinikSection.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.A}\">{x.A}</option>";
                    }
                    return r;
                }
            }
        }
        
        public static string Other_Section_Pelayanan
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetPelayananSection.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.A}\">{x.A}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Other_PaketBHP_Ditagihkan
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetPaketBHPDitagihkan.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Kode}\">{x.A}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Other_Diskon_DenganPetugas
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetDenganPetugas.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Kode}\">{x.Jenis}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Other_OtorisasiUserKhusus_KiteriaOtorisasi
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetKreteriaOtorisasi.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Kreteria}\">{x.Kreteria}</option>";
                    }
                    return r;
                }
            }
        }

        public static string Other_ApprovalUser_Function
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetFunction.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Functi}\">{x.Functi}</option>";
                    }
                    return r;
                }
            }
        }

        #endregion

        #region ===== K A W I

        public static string ListKelompokAkun
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "Biaya", Value = "Biaya" },
                    new SelectListItem(){ Text = "Pendapatan", Value = "Pendapatan" },
                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        public static string ListPostinganKe
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "Hutang", Value = "Hutang" },
                    new SelectListItem(){ Text = "Piutang", Value = "Piutang" },
                    new SelectListItem(){ Text = "GL", Value = "GL" },
                    new SelectListItem(){ Text = "K.S.O", Value = "K.S.O" },

                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        public static string ListHutangKe
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "None", Value = "None" },
                    new SelectListItem(){ Text = "Dokter", Value = "Dokter" },
                    new SelectListItem(){ Text = "Dokter Operator", Value = "DokterOperator" },
                    new SelectListItem(){ Text = "Dokter Asisten", Value = "DokterAsisten" },
                    new SelectListItem(){ Text = "Dokter Anastesi", Value = "DokterAnastesi" },
                    new SelectListItem(){ Text = "Dokter Asisten Anastesi", Value = "DokterAsistenAnastesi" },
                    new SelectListItem(){ Text = "Dokter Anak", Value = "DokterAnak" },
                    new SelectListItem(){ Text = "Asisten/Instrumen", Value = "Asisten/Instrumen" },
                    new SelectListItem(){ Text = "On Loop", Value = "OnLoop" },
                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        public static string ListKelompokJasa
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "Jasa Sarana", Value = "JasaSarana" },
                    new SelectListItem(){ Text = "Jasa Pelayanan", Value = "JasaPelayanan" },
                    new SelectListItem(){ Text = "Obat", Value = "Obat" },
                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        public static string ListKelompokGroup
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "ALL", Value = "ALL" },
                    new SelectListItem(){ Text = "R.Inap", Value = "R.Inap" },
                    new SelectListItem(){ Text = "R.Jalan", Value = "R.Jalan" },
                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        public static string ListKelompokPerawatan
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "ALL", Value = "ALL" },
                    new SelectListItem(){ Text = "R.Inap", Value = "R.Inap" },
                    new SelectListItem(){ Text = "R.Jalan", Value = "R.Jalan" },
                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        public static string TipePasien
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "ALL", Value = "ALL" },
                    new SelectListItem(){ Text = "EXCECUTIVE", Value = "EXCECUTIVE" },
                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        #endregion

        #region ===== P A N C A

        public static string Gender
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "", Value = "" },
                    new SelectListItem(){ Text = "Male", Value = "M" },
                    new SelectListItem(){ Text = "Female", Value = "F" },
                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        public static string Hubungan
        {
            get
            {
                var m = new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "", Value = "" },
                    new SelectListItem(){ Text = "Istri/Suami", Value = "Istri/Suami" },
                    new SelectListItem(){ Text = "Anak", Value = "Anak" },
                };
                string r = "";
                foreach (var x in m)
                {
                    r += $"<option value=\"{x.Value}\">{x.Text}</option>";
                }
                return r;
            }
        }

        public static string Hobby
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.mHobby.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Hobby}\">{x.Hobby}</option>";
                    }
                    return r;
                }
            }
        }

        public static string KategoriVendor
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetKategoriVendor.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Kode_Kategori}\">{x.Kategori_Name}</option>";
                    }
                    return r;
                }
            }
        }

        public static string SpesialisVendor
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetSpesialisVendor.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.SpesialisID.ToString()}\">{x.SpesialisName}</option>";
                    }
                    return r;
                }
            }
        }

        public static string SpesialisVendorVendor
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetSpesialisVendor.ToList(); 
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.SpesialisID.ToString()}\">{x.SpesialisName}</option>";
                    }
                    return r;
                }
            }
        }

        public static string SubSpesialisVendor
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetSubSpesialisVendor.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.SubSpesialisID.ToString()}\">{x.SubSpesialisName}</option>";
                    }
                    return r;
                }
            }
        }

        public static string PersonalVendor
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.ADM_GetPersonalVendor.ToList();
                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.PersonalID.ToString()}\">{x.Nama}</option>";
                    }
                    return r;
                }
            }
        }

        #endregion
    }
}