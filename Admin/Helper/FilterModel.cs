﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.Helper
{
    public class FilterModel
    {
        public string Key { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
    }
}